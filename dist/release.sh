#!/usr/bin/env sh
ls release
SBX_REL_URL=$(curl --request POST --header "PRIVATE-TOKEN: ${CI_RELEASE_TOKEN}" --form "file=@release/LED_Spy_Smash_Box_${VERSION}.hex" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/uploads" |\
    jq -r '.full_path')
HBX_REL_URL=$(curl --request POST --header "PRIVATE-TOKEN: ${CI_RELEASE_TOKEN}" --form "file=@release/LED_Spy_Hit_Box_${VERSION}.hex" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/uploads" |\
    jq -r '.full_path')
cat dist/release_template.json| \
jq  -c \
  --arg sbx ${SBX_REL_URL} \
  --arg hbx ${HBX_REL_URL} \
  --arg vers ${VERSION} \
  '.tag_name=$vers |.name=$vers| .assets.links[0].filepath=$sbx|.assets.links[1].filepath=$hbx' |\
curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: ${CI_RELEASE_TOKEN}" \
  -d@- "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases"
