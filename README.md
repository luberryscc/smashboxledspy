# Welcome to the LED Spy Project

This board is intended to be a one stop solution to input display, and LED control for Hit Box, and Smash Box controllers.
# Support This Project
If you would like to support this project, consider purchasing this board from my [store](https://www.luberryscc.com/product/led-spy-board). Available Soon™.
Or even buying me a beer. [LCC Paypal](paypal.me/LuberrysCC)

## Hit Box PCB

![ledspy-hb](/uploads/b4e4caf630d0754565dc0874e88de52b/ledspy-hb.png)

## Smash Box PCB

![ledspy-sb](/uploads/fc0248a11e4d7d3858ada5726188faba/ledspy-sb.png)

# Using the input viewer

The input viewer portion uses [Open Joystick Display)](https://ojdproject.com), sadly this project is now unmaintained, but works well and has support for my mod board.

## Hit Box

![hb-ojd](/uploads/a817e208dd4a5e300fef7ccb423e71dc/hb-ojd.png)

## Smash Box

![sb-ojd](/uploads/dbdc43699f93d7bb7e0a681710dd77a3/sb-ojd.png)
