// vim: set filetype=arduino :
// RetroSpy Firmware for Arduino
// RetroSpy written by zoggins
// NintendoSpy originally written by jaburns
// Modified for Smash Box and Hit Box by Luberry
#include <ArduinoSTL.h>
#include "config.h"
#include "SmashBoxSpy.h"
#include "kaimana.h"
#include <EEPROM.h>
#define ZERO  '\0'  // Use a byte value of 0x00 to represent a bit with value 0.
#define ONE    '1'  // Use an ASCII one to represent a bit with value 1.  This makes Arduino debugging easier.
#define SPLIT '\n' // Use a new-line character to split up the controller state packets.

#define PROFILE_STATIC 0
#define PROFILE_REACTIVE 1
#define E_INIT 4095
#define E_INIT_VALUE 'T'
#define FLOAT(x) {x/1000000.0}
#define LONG(x) {x*1000000}
#ifndef CFG_VERSION
#define CFG_VERSION 1
#endif
//#define DEBUG


typedef struct kaimanaCfg {
    long bright : 32, max_bright : 32;
#ifndef HITBOX
    int8_t sw_prof0 : 8, sw_prof1 : 8, sw_prof2 : 8;
#endif
} kaimanaCfg;

// config struct for storing profiles on eeprom
typedef struct reactiveCfg {
    long breathe_interval : 32;
    byte aa_r : 8, aa_g : 8, aa_b : 8;
    byte ab_r : 8, ab_g : 8, ab_b : 8;
    byte ac_r : 8, ac_g : 8, ac_b : 8;
    byte ad_r : 8, ad_g : 8, ad_b : 8;

    byte pa_r : 8, pa_g : 8, pa_b : 8;
    byte pb_r : 8, pb_g : 8, pb_b : 8;
    byte pc_r : 8, pc_g : 8, pc_b : 8;
    byte pd_r : 8, pd_g : 8, pd_b : 8;

    byte ka_r : 8, ka_g : 8, ka_b : 8;
    byte kb_r : 8, kb_g : 8, kb_b : 8;
    byte kc_r : 8, kc_g : 8, kc_b : 8;
    byte kd_r : 8, kd_g : 8, kd_b : 8;

    byte ca_r : 8, ca_g : 8, ca_b : 8;
    byte cb_r : 8, cb_g : 8, cb_b : 8;
    byte cc_r : 8, cc_g : 8, cc_b : 8;
    byte cd_r : 8, cd_g : 8, cd_b : 8;
    byte ce_r : 8, ce_g : 8, ce_b : 8;

    byte ma_r : 8, ma_g : 8, ma_b : 8;
    byte mb_r : 8, mb_g : 8, mb_b : 8;
    byte mc_r : 8, mc_g : 8, mc_b : 8;
    byte md_r : 8, md_g : 8, md_b : 8;
    byte me_r : 8, me_g : 8, me_b : 8;

    byte st_r : 8, st_g : 8, st_b : 8;

    byte aa_r_p : 8, aa_g_p : 8, aa_b_p : 8;
    byte ab_r_p : 8, ab_g_p : 8, ab_b_p : 8;
    byte ac_r_p : 8, ac_g_p : 8, ac_b_p : 8;
    byte ad_r_p : 8, ad_g_p : 8, ad_b_p : 8;

    byte pa_r_p : 8, pa_g_p : 8, pa_b_p : 8;
    byte pb_r_p : 8, pb_g_p : 8, pb_b_p : 8;
    byte pc_r_p : 8, pc_g_p : 8, pc_b_p : 8;
    byte pd_r_p : 8, pd_g_p : 8, pd_b_p : 8;

    byte ka_r_p : 8, ka_g_p : 8, ka_b_p : 8;
    byte kb_r_p : 8, kb_g_p : 8, kb_b_p : 8;
    byte kc_r_p : 8, kc_g_p : 8, kc_b_p : 8;
    byte kd_r_p : 8, kd_g_p : 8, kd_b_p : 8;

    byte ca_r_p : 8, ca_g_p : 8, ca_b_p : 8;
    byte cb_r_p : 8, cb_g_p : 8, cb_b_p : 8;
    byte cc_r_p : 8, cc_g_p : 8, cc_b_p : 8;
    byte cd_r_p : 8, cd_g_p : 8, cd_b_p : 8;
    byte ce_r_p : 8, ce_g_p : 8, ce_b_p : 8;

    byte ma_r_p : 8, ma_g_p : 8, ma_b_p : 8;
    byte mb_r_p : 8, mb_g_p : 8, mb_b_p : 8;
    byte mc_r_p : 8, mc_g_p : 8, mc_b_p : 8;
    byte md_r_p : 8, md_g_p : 8, md_b_p : 8;
    byte me_r_p : 8, me_g_p : 8, me_b_p : 8;

    byte st_r_p : 8, st_g_p : 8, st_b_p;
} reactiveCfg;
typedef struct staticCfg {
    long breathe_interval : 32;
    byte aa_r : 8, aa_g : 8, aa_b : 8;
    byte ab_r : 8, ab_g : 8, ab_b : 8;
    byte ac_r : 8, ac_g : 8, ac_b : 8;
    byte ad_r : 8, ad_g : 8, ad_b : 8;

    byte pa_r : 8, pa_g : 8, pa_b : 8;
    byte pb_r : 8, pb_g : 8, pb_b : 8;
    byte pc_r : 8, pc_g : 8, pc_b : 8;
    byte pd_r : 8, pd_g : 8, pd_b : 8;

    byte ka_r : 8, ka_g : 8, ka_b : 8;
    byte kb_r : 8, kb_g : 8, kb_b : 8;
    byte kc_r : 8, kc_g : 8, kc_b : 8;
    byte kd_r : 8, kd_g : 8, kd_b : 8;

    byte ca_r : 8, ca_g : 8, ca_b : 8;
    byte cb_r : 8, cb_g : 8, cb_b : 8;
    byte cc_r : 8, cc_g : 8, cc_b : 8;
    byte cd_r : 8, cd_g : 8, cd_b : 8;
    byte ce_r : 8, ce_g : 8, ce_b : 8;

    byte ma_r : 8, ma_g : 8, ma_b : 8;
    byte mb_r : 8, mb_g : 8, mb_b : 8;
    byte mc_r : 8, mc_g : 8, mc_b : 8;
    byte md_r : 8, md_g : 8, md_b : 8;
    byte me_r : 8, me_g : 8, me_b : 8;

    byte st_r : 8, st_g : 8, st_b : 8;
} staticCfg;



SmashBox smashBoxController(
    SB_PIN_AA,
    SB_PIN_AB,
    SB_PIN_AC,
    SB_PIN_AD,

    SB_PIN_PA,
    SB_PIN_PB,
    SB_PIN_PC,
    SB_PIN_PD,

    SB_PIN_KA,
    SB_PIN_KB,
    SB_PIN_KC,
    SB_PIN_KD,

    SB_PIN_CA,
    SB_PIN_CB,
    SB_PIN_CC,
    SB_PIN_CD,
    SB_PIN_CE,

    SB_PIN_MA,
    SB_PIN_MB,
    SB_PIN_MC,
    SB_PIN_MD,
    SB_PIN_ME,

    SB_PIN_ST,

    SB_PIN_DP,
    SB_PIN_MODE1,
    SB_PIN_MODE3);
Kaimana *kaimanaController;

long currentSBState = 0L;
long lastSBState = 0L;
void setup()
{
    Serial.begin( 115200 );
    config_from_eeprom();
}

inline void sendRawSmashBoxData()
{
#ifndef DEBUG
    // +1 is for the extra mode pin
    for (unsigned char i = 0; i < SB_INPUT_PINS+1; ++i)
    {
        Serial.write(currentSBState & (1L << i) ? ONE : ZERO );
    }
    Serial.write( SPLIT );
#else
    if (currentSBState != lastSBState)
    {
        Serial.print((currentSBState & SB_BTN_AA ) ? "L" : "0");
        Serial.print((currentSBState & SB_BTN_AB ) ? "D" : "0");
        Serial.print((currentSBState & SB_BTN_AC ) ? "R" : "0");
        Serial.print((currentSBState & SB_BTN_AD ) ? "U" : "0");

        Serial.print((currentSBState & SB_BTN_PA ) ? "I" : "0");
        Serial.print((currentSBState & SB_BTN_PB ) ? "X" : "0");
        Serial.print((currentSBState & SB_BTN_PC ) ? "Z" : "0");
        Serial.print((currentSBState & SB_BTN_PD ) ? "P" : "0");

        Serial.print((currentSBState & SB_BTN_KA ) ? "A" : "0");
        Serial.print((currentSBState & SB_BTN_KB ) ? "Y" : "0");
        Serial.print((currentSBState & SB_BTN_KC ) ? "B" : "0");
        Serial.print((currentSBState & SB_BTN_KD ) ? "C" : "0");

        Serial.print((currentSBState & SB_BTN_CA ) ? "V" : "0");
        Serial.print((currentSBState & SB_BTN_CB ) ? "G" : "0");
        Serial.print((currentSBState & SB_BTN_CC ) ? "W" : "0");
        Serial.print((currentSBState & SB_BTN_CD ) ? "Q" : "0");
        Serial.print((currentSBState & SB_BTN_CE ) ? "M" : "0");


        Serial.print((currentSBState & SB_BTN_MA ) ? "T" : "0");
        Serial.print((currentSBState & SB_BTN_MB ) ? "H" : "0");
        Serial.print((currentSBState & SB_BTN_MC ) ? "K" : "0");
        Serial.print((currentSBState & SB_BTN_MD ) ? "J" : "0");
        Serial.print((currentSBState & SB_BTN_ME ) ? "O" : "0");

        Serial.print((currentSBState & SB_BTN_ST ) ? "S" : "0");

        Serial.print((currentSBState & SB_BTN_DP ) ? "#" : "0");
        Serial.print((currentSBState & SB_BTN_MODE1 ) ? "1" : "0");
        Serial.print((!((currentSBState & SB_BTN_MODE1 ) || (currentSBState & SB_BTN_MODE3 ))) ? "2" : "0");
        Serial.print((currentSBState & SB_BTN_MODE3 ) ? "3" : "0");
        Serial.print("\n");
        lastSBState = currentSBState;
    }
#endif
}
inline void loop_SmashBox()
{
    currentSBState = smashBoxController.getState();
    sendRawSmashBoxData();
    kaimanaController->run(currentSBState);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Arduino sketch main loop definition.
void loop()
{
    loop_SmashBox();
}
inline void config_from_eeprom()
{
    initialize_eeprom_config();
    int read_addr = 0;
    std::vector<Profile*> profiles;
    uint16_t version;
    EEPROM.get(read_addr,version);
    read_addr+=sizeof(version);
    byte mappings[NUM_LEDS/2];
    for (int i = 0; i<NUM_LEDS/2;i++,read_addr++){
    mappings[i] = EEPROM.read(read_addr);
    #ifdef DEBUG
    Serial.println(mappings[i]);
    #endif
    }
    kaimanaCfg kcfg;
    EEPROM.get(read_addr,kcfg);

#ifdef DEBUG
    Serial.println("version");
    Serial.println(version);
    Serial.println("kcfg");
    Serial.println(FLOAT(kcfg.bright));
    Serial.println(FLOAT(kcfg.max_bright));
    Serial.println(kcfg.sw_prof0);
    Serial.println(kcfg.sw_prof1);
    Serial.println(kcfg.sw_prof2);
#endif

    read_addr += sizeof(kaimanaCfg);
    uint8_t numProfiles=EEPROM.read(read_addr);
#ifdef DEBUG
    Serial.println("profiles");
    Serial.println(numProfiles);
#endif
    read_addr+=sizeof(uint8_t);
    for (uint8_t i = 0; i<numProfiles; i++) {
        uint8_t ptype;
        ptype = EEPROM.read(read_addr);
#ifdef DEBUG
        Serial.println("ptype");
        Serial.println(ptype);
#endif
        read_addr+=sizeof(uint8_t);
        switch(ptype) {
        case PROFILE_STATIC:
            staticCfg cfg;
            EEPROM.get(read_addr,cfg);

            profiles.push_back(
                new Static(
                    FLOAT(cfg.breathe_interval),

                    cfg.aa_r, cfg.aa_g, cfg.aa_b,
                    cfg.ab_r, cfg.ab_g, cfg.ab_b,
                    cfg.ac_r, cfg.ac_g, cfg.ac_b,
                    cfg.ad_r, cfg.ad_g, cfg.ad_b,

                    cfg.pa_r, cfg.pa_g, cfg.pa_b,
                    cfg.pb_r, cfg.pb_g, cfg.pb_b,
                    cfg.pc_r, cfg.pc_g, cfg.pc_b,
                    cfg.pd_r, cfg.pd_g, cfg.pd_b,

                    cfg.ka_r, cfg.ka_g, cfg.ka_b,
                    cfg.kb_r, cfg.kb_g, cfg.kb_b,
                    cfg.kc_r, cfg.kc_g, cfg.kc_b,
                    cfg.kd_r, cfg.kd_g, cfg.kd_b,

                    cfg.ca_r, cfg.ca_g, cfg.ca_b,
                    cfg.cb_r, cfg.cb_g, cfg.cb_b,
                    cfg.cc_r, cfg.cc_g, cfg.cc_b,
                    cfg.cd_r, cfg.cd_g, cfg.cd_b,
                    cfg.ce_r, cfg.ce_g, cfg.ce_b,

                    cfg.ma_r, cfg.ma_g, cfg.ma_b,
                    cfg.mb_r, cfg.mb_g, cfg.mb_b,
                    cfg.mc_r, cfg.mc_g, cfg.mc_b,
                    cfg.md_r, cfg.md_g, cfg.md_b,
                    cfg.me_r, cfg.me_g, cfg.me_b,

                    cfg.st_r, cfg.st_g, cfg.st_b,

                    mappings
                )
            );
            read_addr+=sizeof(staticCfg);
            break;
        case PROFILE_REACTIVE:
            reactiveCfg rcfg;
            EEPROM.get(read_addr,rcfg);
            profiles.push_back(
                new Reactive(
                    FLOAT(rcfg.breathe_interval),

                    rcfg.aa_r, rcfg.aa_g, rcfg.aa_b,
                    rcfg.ab_r, rcfg.ab_g, rcfg.ab_b,
                    rcfg.ac_r, rcfg.ac_g, rcfg.ac_b,
                    rcfg.ad_r, rcfg.ad_g, rcfg.ad_b,

                    rcfg.pa_r, rcfg.pa_g, rcfg.pa_b,
                    rcfg.pb_r, rcfg.pb_g, rcfg.pb_b,
                    rcfg.pc_r, rcfg.pc_g, rcfg.pc_b,
                    rcfg.pd_r, rcfg.pd_g, rcfg.pd_b,

                    rcfg.ka_r, rcfg.ka_g, rcfg.ka_b,
                    rcfg.kb_r, rcfg.kb_g, rcfg.kb_b,
                    rcfg.kc_r, rcfg.kc_g, rcfg.kc_b,
                    rcfg.kd_r, rcfg.kd_g, rcfg.kd_b,

                    rcfg.ca_r, rcfg.ca_g, rcfg.ca_b,
                    rcfg.cb_r, rcfg.cb_g, rcfg.cb_b,
                    rcfg.cc_r, rcfg.cc_g, rcfg.cc_b,
                    rcfg.cd_r, rcfg.cd_g, rcfg.cd_b,
                    rcfg.ce_r, rcfg.ce_g, rcfg.ce_b,

                    rcfg.ma_r, rcfg.ma_g, rcfg.ma_b,
                    rcfg.mb_r, rcfg.mb_g, rcfg.mb_b,
                    rcfg.mc_r, rcfg.mc_g, rcfg.mc_b,
                    rcfg.md_r, rcfg.md_g, rcfg.md_b,
                    rcfg.me_r, rcfg.me_g, rcfg.me_b,

                    rcfg.st_r, rcfg.st_g, rcfg.st_b,

                    rcfg.aa_r_p, rcfg.aa_g_p, rcfg.aa_b_p,
                    rcfg.ab_r_p, rcfg.ab_g_p, rcfg.ab_b_p,
                    rcfg.ac_r_p, rcfg.ac_g_p, rcfg.ac_b_p,
                    rcfg.ad_r_p, rcfg.ad_g_p, rcfg.ad_b_p,

                    rcfg.pa_r_p, rcfg.pa_g_p, rcfg.pa_b_p,
                    rcfg.pb_r_p, rcfg.pb_g_p, rcfg.pb_b_p,
                    rcfg.pc_r_p, rcfg.pc_g_p, rcfg.pc_b_p,
                    rcfg.pd_r_p, rcfg.pd_g_p, rcfg.pd_b_p,

                    rcfg.ka_r_p, rcfg.ka_g_p, rcfg.ka_b_p,
                    rcfg.kb_r_p, rcfg.kb_g_p, rcfg.kb_b_p,
                    rcfg.kc_r_p, rcfg.kc_g_p, rcfg.kc_b_p,
                    rcfg.kd_r_p, rcfg.kd_g_p, rcfg.kd_b_p,

                    rcfg.ca_r_p, rcfg.ca_g_p, rcfg.ca_b_p,
                    rcfg.cb_r_p, rcfg.cb_g_p, rcfg.cb_b_p,
                    rcfg.cc_r_p, rcfg.cc_g_p, rcfg.cc_b_p,
                    rcfg.cd_r_p, rcfg.cd_g_p, rcfg.cd_b_p,
                    rcfg.ce_r_p, rcfg.ce_g_p, rcfg.ce_b_p,

                    rcfg.ma_r_p, rcfg.ma_g_p, rcfg.ma_b_p,
                    rcfg.mb_r_p, rcfg.mb_g_p, rcfg.mb_b_p,
                    rcfg.mc_r_p, rcfg.mc_g_p, rcfg.mc_b_p,
                    rcfg.md_r_p, rcfg.md_g_p, rcfg.md_b_p,
                    rcfg.me_r_p, rcfg.me_g_p, rcfg.me_b_p,

                    rcfg.st_r_p, rcfg.st_g_p, rcfg.st_b_p,
                    mappings
                )
            );
            read_addr+=sizeof(reactiveCfg);
            break;
        }

    }
    // Hit Box doesnt have the profile switch so for the HB version we omit the defaults
    // for profile switching
#ifdef HITBOX
    kaimanaController=new Kaimana(FLOAT(kcfg.bright),FLOAT(kcfg.max_bright),profiles,mappings);
#else
    kaimanaController=new Kaimana(FLOAT(kcfg.bright),FLOAT(kcfg.max_bright),profiles,kcfg.sw_prof0,kcfg.sw_prof1,kcfg.sw_prof2,mappings);
#endif
}

inline void initialize_eeprom_config()
{
   if (EEPROM.read(E_INIT) == E_INIT_VALUE) {
  uint16_t vers;
    EEPROM.get(0,vers);
    if (vers == CFG_VERSION){
        return;
    }
    }
#ifdef DEBUG
    Serial.println("Writing");
#endif
byte mappings[NUM_LEDS/2];

    int write_addr = 0;
    uint16_t version = CFG_VERSION;
    EEPROM.put(write_addr,version);
    write_addr+=sizeof(version);
#ifndef HITBOX
//LED INDEX
    mappings[0]=20;
    mappings[1]=19;
    mappings[2]=17;
    mappings[3]=18;

 
    mappings[4]=10;
    mappings[5]=9;
    mappings[6]=8;
    mappings[7]=7;
    mappings[8]=6;
    mappings[9]=5;
    mappings[10]=4;
    mappings[11]=3;

 
    mappings[12]=11;
    mappings[13]=2;
    mappings[14]=12;
    mappings[15]=1;
    mappings[16]=0;

 
    mappings[17]=21;
    mappings[18]=15;
    mappings[19]=16;
    mappings[20]=14;
    mappings[21]=13;

 
    mappings[22]=22;

#else
// Hit Box LED Values
    mappings[0]=0;
    mappings[1]=1;
    mappings[2]=2;
    mappings[3]=3;


    mappings[4]=5;
    mappings[5]=7;
    mappings[6]=9;
    mappings[7]=11;
    mappings[8]=10;
    mappings[9]=8;
    mappings[10]=6;
    mappings[11]=4;


    mappings[12]=19;
    mappings[13]=18;
    mappings[14]=20;
    mappings[15]=16;
    mappings[16]=17;


    mappings[17]=14;
    mappings[18]=12;
    mappings[19]=13;
    mappings[20]=21;
    mappings[21]=22;


    mappings[22]=15;

#endif
    for (int i = 0; i<NUM_LEDS/2;i++,write_addr++){
    EEPROM.update(write_addr,mappings[i]);
    }
    kaimanaCfg kcfg;
    float bright,max_bright;
    kcfg.bright = LONG(0.5);
    kcfg.max_bright =LONG(0.1);
#ifndef HITBOX
    kcfg.sw_prof0 = 0;
    kcfg.sw_prof1 = 1;
    kcfg.sw_prof2 = 2;
#endif
    EEPROM.put(write_addr,kcfg);
    write_addr+=sizeof(kaimanaCfg);
    EEPROM.update(write_addr, 3);
    write_addr+=sizeof(byte);

    long react = LONG(0.0);
    EEPROM.update(write_addr, PROFILE_STATIC);
    write_addr+=sizeof(byte);
#ifdef HITBOX
    EEPROM.put(write_addr,staticCfg {STATIC_HITBOX(react,COLOR_WHITE,COLOR_RED)});
#else
    EEPROM.put(write_addr,staticCfg {STATIC_SMASHBOX(react,COLOR_WHITE,COLOR_RED,COLOR_YELLOW)});
#endif
    write_addr+=sizeof(staticCfg);

    EEPROM.update(write_addr, PROFILE_STATIC);
    write_addr+=sizeof(byte);
#ifdef HITBOX
    EEPROM.put(write_addr,staticCfg {STATIC_HITBOX(react,COLOR_WHITE,COLOR_RED)});
#else
    EEPROM.put(write_addr,staticCfg {STATIC_SMASHBOX(react,COLOR_WHITE,COLOR_RED,COLOR_YELLOW)});
#endif
    write_addr+=sizeof(staticCfg);

    EEPROM.update(write_addr, PROFILE_REACTIVE);
    write_addr+=sizeof(byte);
    EEPROM.put(write_addr,reactiveCfg{REACTIVE_HITBOX(react,COLOR_WHITE,COLOR_RED,COLOR_RED,COLOR_WHITE)});
    write_addr+=sizeof(reactiveCfg);
    EEPROM.update(E_INIT,E_INIT_VALUE);
}
