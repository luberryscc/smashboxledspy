// vim: set filetype=arduino :
#ifndef SmashBox_h
#define SmashBox_h
#include "config.h"

//declares all button integer positions.
//A the analog directional buttons (left, down, right, up)
//P the top row (standard fightstick notation)
//K the bottom row (standard fightstick notation)
//C the the c-stick area, left to tright, top to bottom
//M the modifier area, left to right, top to bottom
enum
{
    SB_BTN_AA = 1L,
    SB_BTN_AB = 2L,
    SB_BTN_AC = 4L,
    SB_BTN_AD = 8L,

    SB_BTN_PA = 16L,
    SB_BTN_PB = 32L,
    SB_BTN_PC = 64L,
    SB_BTN_PD = 128L,

    SB_BTN_KA = 256L,
    SB_BTN_KB = 512L,
    SB_BTN_KC = 1024L,
    SB_BTN_KD = 2048L,

    SB_BTN_CA = 4096L,
    SB_BTN_CB = 8192L,
    SB_BTN_CC = 16384L,
    SB_BTN_CD = 32768L,
    SB_BTN_CE = 65536L,

    SB_BTN_MA = 131072L,
    SB_BTN_MB = 262144L,
    SB_BTN_MC = 524288L,
    SB_BTN_MD = 1048576L,
    SB_BTN_ME = 2097152L,

    SB_BTN_ST = 4194304L,

    SB_BTN_DP = 8388608L,
    SB_BTN_MODE1 = 16777216L,
    SB_BTN_MODE2 = 33554432L,
    SB_BTN_MODE3 = 67108864L

};

//declares all pins.
//A the analog directional buttons (left, down, right, up)
//P the top row (standard fightstick notation)
//K the bottom row (standard fightstick notation)
//C the the c-stick area, left to tright, top to bottom
//M the modifier area, left to right, top to bottom
enum
{
    SB_PIN_AA = 24,
    SB_PIN_AB = 23,
    SB_PIN_AC = 25,
    SB_PIN_AD = 22,

    SB_PIN_PA = 41,
    SB_PIN_PB = 43,
    SB_PIN_PC = 40,
    SB_PIN_PD = 12,

    SB_PIN_KA = 44,
    SB_PIN_KB = 42,
    SB_PIN_KC = 7,
    SB_PIN_KD = 45,

    SB_PIN_CA = 34,
    SB_PIN_CB = 37,
    SB_PIN_CC = 36,
    SB_PIN_CD = 35,
    SB_PIN_CE = 46,

    SB_PIN_MA = 47,
    SB_PIN_MB = 28,
    SB_PIN_MC = 30,
    SB_PIN_MD = 29,
    SB_PIN_ME = 31,

#ifdef BOARD_REV_0_1
    // Rev 0.1 pinout
    SB_PIN_ST = 51,
#else
    // Rev >= 0.2 pinout
    SB_PIN_ST = 50,
#endif

#ifdef BOARD_REV_0_2
    // Rev 0.2 pinout
    SB_PIN_DP = 49,
#else
    // Rev >= 0.3 pinout
    SB_PIN_DP = 51,
#endif
    SB_PIN_MODE1 = 38,
    SB_PIN_MODE3 = 39

};

const byte SB_INPUT_PINS = 26;

const unsigned long SB_READ_DELAY_MS = 10;

class SmashBox {
public:
    SmashBox(
        byte sb_pin_aa,
        byte sb_pin_ab,
        byte sb_pin_ac,
        byte sb_pin_ad,

        byte sb_pin_pa,
        byte sb_pin_pb,
        byte sb_pin_pc,
        byte sb_pin_pd,

        byte sb_pin_ka,
        byte sb_pin_kb,
        byte sb_pin_kc,
        byte sb_pin_kd,

        byte sb_pin_ca,
        byte sb_pin_cb,
        byte sb_pin_cc,
        byte sb_pin_cd,
        byte sb_pin_ce,

        byte sb_pin_ma,
        byte sb_pin_mb,
        byte sb_pin_mc,
        byte sb_pin_md,
        byte sb_pin_me,

        byte sb_pin_st,

        byte sb_pin_dp,
        byte sb_pin_mode1,
        byte sb_pin_mode3);

    long getState();

private:
    void readCycle();

    long _currentState;

    unsigned long _lastReadTime;

    byte _inputPins[SB_INPUT_PINS];
};

#endif
