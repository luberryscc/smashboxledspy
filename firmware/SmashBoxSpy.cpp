// vim: set filetype=arduino :
#include "Arduino.h"
#include "config.h"
#include "SmashBoxSpy.h"


SmashBox::SmashBox(
    byte sb_pin_aa,
    byte sb_pin_ab,
    byte sb_pin_ac,
    byte sb_pin_ad,

    byte sb_pin_pa,
    byte sb_pin_pb,
    byte sb_pin_pc,
    byte sb_pin_pd,

    byte sb_pin_ka,
    byte sb_pin_kb,
    byte sb_pin_kc,
    byte sb_pin_kd,

    byte sb_pin_ca,
    byte sb_pin_cb,
    byte sb_pin_cc,
    byte sb_pin_cd,
    byte sb_pin_ce,

    byte sb_pin_ma,
    byte sb_pin_mb,
    byte sb_pin_mc,
    byte sb_pin_md,
    byte sb_pin_me,

    byte sb_pin_st,

    byte sb_pin_dp,
    byte sb_pin_mode1,
    byte sb_pin_mode3)
{
    // Set pins
    _inputPins[0] = sb_pin_aa  ;
    _inputPins[1] = sb_pin_ab  ;
    _inputPins[2] = sb_pin_ac ;
    _inputPins[3] = sb_pin_ad    ;

    _inputPins[4] = sb_pin_pa ;
    _inputPins[5] = sb_pin_pb ;
    _inputPins[6] = sb_pin_pc ;
    _inputPins[7] = sb_pin_pd ;

    _inputPins[8] = sb_pin_ka ;
    _inputPins[9] = sb_pin_kb ;
    _inputPins[10] = sb_pin_kc ;
    _inputPins[11] = sb_pin_kd ;

    _inputPins[12] = sb_pin_ca ;
    _inputPins[13] = sb_pin_cb ;
    _inputPins[14] = sb_pin_cc ;
    _inputPins[15] = sb_pin_cd ;
    _inputPins[16] = sb_pin_ce;

    _inputPins[17] = sb_pin_ma ;
    _inputPins[18] = sb_pin_mb ;
    _inputPins[19] = sb_pin_mc   ;
    _inputPins[20] = sb_pin_md ;
    _inputPins[21] = sb_pin_me ;

    _inputPins[22] = sb_pin_st ;

    _inputPins[23] = sb_pin_dp;
    _inputPins[24] = sb_pin_mode1;
    _inputPins[25] = sb_pin_mode3;

    int pin_mode=INPUT_PULLUP;
    // Setup input pins
    for (byte i = 0; i < SB_INPUT_PINS; i++)
    {
        pinMode(_inputPins[i], pin_mode);
    }

    _currentState = 0;
    _lastReadTime = millis();
}

long SmashBox::getState()
{
    if (max(millis() - _lastReadTime, 0) < SB_READ_DELAY_MS)
    {
        // Not enough time has elapsed, return previously read state
        return _currentState;
    }

    noInterrupts();

    // Clear current state
    _currentState = 0L;

    readCycle();

    interrupts();

    _lastReadTime = millis();

    return _currentState;
}

void SmashBox::readCycle()
{
    bool m2 = true;
    // Read input pins for Up, Down, Left, Right, 1, 2, 3
    if (digitalRead(_inputPins[0] ) == LOW) {
        _currentState |= SB_BTN_AA ;
    }
    if (digitalRead(_inputPins[1] ) == LOW) {
        _currentState |= SB_BTN_AB ;
    }
    if (digitalRead(_inputPins[2] ) == LOW) {
        _currentState |= SB_BTN_AC ;
    }
    if (digitalRead(_inputPins[3] ) == LOW) {
        _currentState |= SB_BTN_AD ;
    }
    if (digitalRead(_inputPins[4] ) == LOW) {
        _currentState |= SB_BTN_PA ;
    }
    if (digitalRead(_inputPins[5] ) == LOW) {
        _currentState |= SB_BTN_PB ;
    }
    if (digitalRead(_inputPins[6] ) == LOW) {
        _currentState |= SB_BTN_PC ;
    }
    if (digitalRead(_inputPins[7] ) == LOW) {
        _currentState |= SB_BTN_PD ;
    }
    if (digitalRead(_inputPins[8] ) == LOW) {
        _currentState |= SB_BTN_KA ;
    }
    if (digitalRead(_inputPins[9] ) == LOW) {
        _currentState |= SB_BTN_KB ;
    }
    if (digitalRead(_inputPins[10]) == LOW) {
        _currentState |= SB_BTN_KC ;
    }
    if (digitalRead(_inputPins[11]) == LOW) {
        _currentState |= SB_BTN_KD ;
    }
    if (digitalRead(_inputPins[12]) == LOW) {
        _currentState |= SB_BTN_CA ;
    }
    if (digitalRead(_inputPins[13]) == LOW) {
        _currentState |= SB_BTN_CB ;
    }
    if (digitalRead(_inputPins[14]) == LOW) {
        _currentState |= SB_BTN_CC ;
    }
    if (digitalRead(_inputPins[15]) == LOW) {
        _currentState |= SB_BTN_CD ;
    }
    if (digitalRead(_inputPins[16]) == LOW) {
        _currentState |= SB_BTN_CE ;
    }
    if (digitalRead(_inputPins[17]) == LOW) {
        _currentState |= SB_BTN_MA ;
    }
    if (digitalRead(_inputPins[18]) == LOW) {
        _currentState |= SB_BTN_MB ;
    }
    if (digitalRead(_inputPins[19]) == LOW) {
        _currentState |= SB_BTN_MC ;
    }
    if (digitalRead(_inputPins[20]) == LOW) {
        _currentState |= SB_BTN_MD ;
    }
    if (digitalRead(_inputPins[21]) == LOW) {
        _currentState |= SB_BTN_ME ;
    }
    if (digitalRead(_inputPins[22]) == LOW) {
        _currentState |= SB_BTN_ST ;
    }
    if (digitalRead(_inputPins[23]) == LOW) {
        _currentState |= SB_BTN_DP ;
    }
    if (digitalRead(_inputPins[24]) == LOW) {
        _currentState |= SB_BTN_MODE1 ;
        m2 = false;
    }
    if (digitalRead(_inputPins[25]) == LOW) {
        _currentState |= SB_BTN_MODE3 ;
        m2 = false;
    }
#ifndef HITBOX
    if (m2) {
        _currentState |= SB_BTN_MODE2 ;
    }
#endif
}
