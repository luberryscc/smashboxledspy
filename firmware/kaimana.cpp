// vim: set filetype=arduino :
#include "config.h"
#include "kaimana.h"
#include "FastLED.h"
#include "SmashBoxSpy.h"
#include <ArduinoSTL.h>

#ifdef HITBOX
/**
  -----------------------------------------------------
  Hit Box constructor for the Kaimana object.
  float brightness       : the default brightness of the LEDS. can range from 0.0-1.0
  float max_brightness   : the max brightness of the leds. can range from 0.0-1.0
  std::vector<Profile*> p: is the list of available profiles that can be toggled between
  -----------------------------------------------------
**/
Kaimana::Kaimana( float brightness, float max_brightness, std::vector<Profile*>p,byte mp[NUM_LEDS/2]) {
// Hit Box will always default to zero for lighting profiles
// as it doesnt have a built in profile switch
    currentProfileID = 0;
#else
/**
  -----------------------------------------------------
  Smash Box constructor for the Kaimana object.
  float brightness       : the default brightness of the LEDS. can range from 0.0-1.0
  float max_brightness   : the max brightness of the leds. can range from 0.0-1.0
  std::vector<Profile*> p: is the list of available profiles that can be toggled between
  int8_t sw_profile_1    : the default profile for profile switch position 1. Should
                           be a valid index of the supplied vector of profiles
  int8_t sw_profile_2    : the default profile for profile switch position 2. Should
                           be a valid index of the supplied vector of profiles
  int8_t sw_profile_3    : the default profile for profile switch position 3. Should
                           be a valid index of the supplied vector of profiles
  -----------------------------------------------------
**/
Kaimana::Kaimana( float brightness, float max_brightness, std::vector<Profile*>p,int8_t sw_profile_1,int8_t sw_profile_2,int8_t sw_profile_3,
    byte mp[NUM_LEDS/2]
) {
    // Set up switch profiles and defaults for profile switch position
    sw_profiles[0]=sw_profile_1;
    sw_profiles[1]=sw_profile_2;
    sw_profiles[2]=sw_profile_3;
    currentProfileID = -1;
    lastProfileSwitchState=-1;
#endif
    profiles = p;
    bright = brightness;
    max_bright = max_brightness;
    defaultBrightness = brightness;
    comeDown = true;
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
    for(int i =0; i<NUM_LEDS/2; i++){
    mappings[i] = mp[i];
    }
}

/**
  -----------------------------------------------------
  Set both LEDs for the given button (lower index) to the provided colors
  -----------------------------------------------------
*/
void Kaimana::setLEDs(int index, byte iR, byte iG, byte iB) {
    int id = index*2;
    leds[id].r = iR * bright;
    leds[id].g = iG * bright;
    leds[id].b = iB * bright;
    if ((id + 1) < NUM_LEDS) {
        leds[id + 1].r = iR * bright;
        leds[id + 1].g = iG * bright;
        leds[id + 1].b = iB * bright;
    }
}

/**
  -----------------------------------------------------
  Set every LED in the sequence at once
  -----------------------------------------------------
*/
void Kaimana::setALL(byte iR, byte iG, byte iB) {
    for (int i = 0; i < NUM_LEDS; i++) {
        setLEDs(i, iR, iG, iB );
    }
    FastLED.show();
}


/**
  -----------------------------------------------------
  breath will modify the values of the leds to adjust the brightness for a breathing function
  -----------------------------------------------------
*/
void Kaimana::breathe(float interval) {
    if (interval == 0) {
        if (bright == defaultBrightness) {
            return;
        }
        bright = defaultBrightness;
        FastLED.delay(3);
        return;
    }

    if(!comeDown) {
        bright+=interval;
        if(bright >= max_bright) {
            comeDown = true;
            bright = max_bright;
        }
    }
    else {
        bright-=interval;
        if(bright <= 0) {
            comeDown = false;
            bright =0;
        }
    }
}

#ifndef HITBOX
/**
  -----------------------------------------------------
  profile_id will return the index where the default profile for the switch position is
  by decoding the given state variable, and will return -2 if an invalid mode is decoded
  -----------------------------------------------------
*/
inline int8_t Kaimana::profile_id(long state) {
    if ( state & SB_BTN_MODE1 ) {
        return 0;
    } else if (state &SB_BTN_MODE2) {
        return 1;
    } else if (state &SB_BTN_MODE3) {
        return 2;
    }
    return -2;
}
#endif

/**
  -----------------------------------------------------
  Led Control Set our LED profiles based on switches/hotkeys as well as brightness,
  takes the state of the switches to allow for hotkeys and such
  -----------------------------------------------------
*/

void Kaimana::run(long state) {
    FastLED.delay(3);

#ifdef HITBOX
    // Handle brightness and profile switching hotkeys for the Hit Box
    if ((state & SB_BTN_MA) && (state & SB_BTN_MB) && (state & SB_BTN_MC) && (state & SB_BTN_ST)) {
        if ((state & SB_BTN_AD) && (bright < max_bright)) {
            bright += 0.1;
            FastLED.delay(80);
        }
        if ((state & SB_BTN_AB) && (bright > 0.1)) {
            bright -= 0.1;
            FastLED.delay(80);
        }
        if ((state & SB_BTN_AC) && (currentProfileID > 0)) {
            currentProfileID--;
            FastLED.delay(150);
        }
        if ((state & SB_BTN_AA) && (currentProfileID < profiles.size())) {
            currentProfileID++;
            FastLED.delay(150);
        }
    }
#else
    // get the profile id from the state variable
    int8_t profileID = profile_id(state);
    // handle an edge case on startup where the profile switch state cannot be read initially
    if (profileID == -2 && currentProfileID == -1) {
        currentProfileID=0;
    }

    // if the profile switch changes values switch to that switch profile's default lighting profile
    if (currentProfileID <0 ||(lastProfileSwitchState != profileID && profileID >=0 ) ) {
        lastProfileSwitchState=profileID;
        currentProfileID=sw_profiles[lastProfileSwitchState];
        FastLED.delay(150);
    }
    // Handle brightness and profile switching hotkeys for the Smash Box
    else if ((state & SB_BTN_MA) && (state & SB_BTN_PC) && (state & SB_BTN_PD)) {
        if ((state & SB_BTN_MB) && (bright < max_bright)) {
            bright += 0.1;
            FastLED.delay(80);
        }
        else if ((state & SB_BTN_ME) && (bright > 0.1)) {
            bright -= 0.1;
            FastLED.delay(80);
        }
        else if ((state & SB_BTN_MD) && (currentProfileID > 0)) {
            FastLED.delay(150);
            currentProfileID--;
        }
        else if ((state & SB_BTN_MC) && (currentProfileID+1 < profiles.size())) {
            FastLED.delay(150);
            currentProfileID++;
        }
    }
#endif
    // allows any profile to breathe as long as it is enabled
    breathe(profiles.at(currentProfileID)->breathe_interval());
    // calls the current profile's run method which will setup the leds
    profiles.at(currentProfileID)->run(this,state);
    FastLED.show();
}


/**
  -----------------------------------------------------
  Reactive Lighting configuration
  float breathe_interval: the amount to increase/decrease the brightness
  byte XX_[r,g,b]       : RGB values for the non pressed state of buttons.
                          The buttons correlating to their defined index
  byte XX_[r,g,b]_p     : RGB values for the pressed state of buttons.
                          The buttons correlating to their defined index
  -----------------------------------------------------
*/
Reactive::Reactive(
    float breathe_interval,
    byte aa_r, byte aa_g, byte aa_b,
    byte ab_r, byte ab_g, byte ab_b,
    byte ac_r, byte ac_g, byte ac_b,
    byte ad_r, byte ad_g, byte ad_b,

    byte pa_r, byte pa_g, byte pa_b,
    byte pb_r, byte pb_g, byte pb_b,
    byte pc_r, byte pc_g, byte pc_b,
    byte pd_r, byte pd_g, byte pd_b,

    byte ka_r, byte ka_g, byte ka_b,
    byte kb_r, byte kb_g, byte kb_b,
    byte kc_r, byte kc_g, byte kc_b,
    byte kd_r, byte kd_g, byte kd_b,

    byte ca_r, byte ca_g, byte ca_b,
    byte cb_r, byte cb_g, byte cb_b,
    byte cc_r, byte cc_g, byte cc_b,
    byte cd_r, byte cd_g, byte cd_b,
    byte ce_r, byte ce_g, byte ce_b,

    byte ma_r, byte ma_g, byte ma_b,
    byte mb_r, byte mb_g, byte mb_b,
    byte mc_r, byte mc_g, byte mc_b,
    byte md_r, byte md_g, byte md_b,
    byte me_r, byte me_g, byte me_b,

    byte st_r, byte st_g, byte st_b,

    byte aa_r_p, byte aa_g_p, byte aa_b_p,
    byte ab_r_p, byte ab_g_p, byte ab_b_p,
    byte ac_r_p, byte ac_g_p, byte ac_b_p,
    byte ad_r_p, byte ad_g_p, byte ad_b_p,

    byte pa_r_p, byte pa_g_p, byte pa_b_p,
    byte pb_r_p, byte pb_g_p, byte pb_b_p,
    byte pc_r_p, byte pc_g_p, byte pc_b_p,
    byte pd_r_p, byte pd_g_p, byte pd_b_p,

    byte ka_r_p, byte ka_g_p, byte ka_b_p,
    byte kb_r_p, byte kb_g_p, byte kb_b_p,
    byte kc_r_p, byte kc_g_p, byte kc_b_p,
    byte kd_r_p, byte kd_g_p, byte kd_b_p,

    byte ca_r_p, byte ca_g_p, byte ca_b_p,
    byte cb_r_p, byte cb_g_p, byte cb_b_p,
    byte cc_r_p, byte cc_g_p, byte cc_b_p,
    byte cd_r_p, byte cd_g_p, byte cd_b_p,
    byte ce_r_p, byte ce_g_p, byte ce_b_p,

    byte ma_r_p, byte ma_g_p, byte ma_b_p,
    byte mb_r_p, byte mb_g_p, byte mb_b_p,
    byte mc_r_p, byte mc_g_p, byte mc_b_p,
    byte md_r_p, byte md_g_p, byte md_b_p,
    byte me_r_p, byte me_g_p, byte me_b_p,

    byte st_r_p, byte st_g_p, byte st_b_p,

    byte mappings[NUM_LEDS/2]
) {
    bInterval=breathe_interval;
    /**
      -----------------------------------------------------
      Store the released state for all buttons
      -----------------------------------------------------
    */
    SET_RGB(colors[LED_A1][STATE_RELEASED],aa_r,aa_g,aa_b);
    SET_RGB(colors[LED_A2][STATE_RELEASED],ab_r,ab_g,ab_b);
    SET_RGB(colors[LED_A3][STATE_RELEASED],ac_r,ac_g,ac_b);
    SET_RGB(colors[LED_A4][STATE_RELEASED],ad_r,ad_g,ad_b);

    SET_RGB(colors[LED_P1][STATE_RELEASED],pa_r,pa_g,pa_b);
    SET_RGB(colors[LED_P2][STATE_RELEASED],pb_r,pb_g,pb_b);
    SET_RGB(colors[LED_P3][STATE_RELEASED],pc_r,pc_g,pc_b);
    SET_RGB(colors[LED_P4][STATE_RELEASED],pd_r,pd_g,pd_b);
    SET_RGB(colors[LED_K1][STATE_RELEASED],ka_r,ka_g,ka_b);
    SET_RGB(colors[LED_K2][STATE_RELEASED],kb_r,kb_g,kb_b);
    SET_RGB(colors[LED_K3][STATE_RELEASED],kc_r,kc_g,kc_b);
    SET_RGB(colors[LED_K4][STATE_RELEASED],kd_r,kd_g,kd_b);

    SET_RGB(colors[LED_C1][STATE_RELEASED],ca_r,ca_g,ca_b);
    SET_RGB(colors[LED_C2][STATE_RELEASED],cb_r,cb_g,cb_b);
    SET_RGB(colors[LED_C3][STATE_RELEASED],cc_r,cc_g,cc_b);
    SET_RGB(colors[LED_C4][STATE_RELEASED],cd_r,cd_g,cd_b);
    SET_RGB(colors[LED_C5][STATE_RELEASED],ce_r,ce_g,ce_b);

    SET_RGB(colors[LED_M1][STATE_RELEASED],ma_r,ma_g,ma_b);
    SET_RGB(colors[LED_M2][STATE_RELEASED],mb_r,mb_g,mb_b);
    SET_RGB(colors[LED_M3][STATE_RELEASED],mc_r,mc_g,mc_b);
    SET_RGB(colors[LED_M4][STATE_RELEASED],md_r,md_g,md_b);
    SET_RGB(colors[LED_M5][STATE_RELEASED],me_r,me_g,me_b);

    SET_RGB(colors[LED_ST][STATE_RELEASED],st_r,st_g,st_b);

    /**
      -----------------------------------------------------
      Store the pressed state for all buttons
      -----------------------------------------------------
    */
    SET_RGB(colors[LED_A1][STATE_PRESSED],aa_r_p,aa_g_p,aa_b_p);
    SET_RGB(colors[LED_A2][STATE_PRESSED],ab_r_p,ab_g_p,ab_b_p);
    SET_RGB(colors[LED_A3][STATE_PRESSED],ac_r_p,ac_g_p,ac_b_p);
    SET_RGB(colors[LED_A4][STATE_PRESSED],ad_r_p,ad_g_p,ad_b_p);

    SET_RGB(colors[LED_P1][STATE_PRESSED],pa_r_p,pa_g_p,pa_b_p);
    SET_RGB(colors[LED_P2][STATE_PRESSED],pb_r_p,pb_g_p,pb_b_p);
    SET_RGB(colors[LED_P3][STATE_PRESSED],pc_r_p,pc_g_p,pc_b_p);
    SET_RGB(colors[LED_P4][STATE_PRESSED],pd_r_p,pd_g_p,pd_b_p);
    SET_RGB(colors[LED_K1][STATE_PRESSED],ka_r_p,ka_g_p,ka_b_p);
    SET_RGB(colors[LED_K2][STATE_PRESSED],kb_r_p,kb_g_p,kb_b_p);
    SET_RGB(colors[LED_K3][STATE_PRESSED],kc_r_p,kc_g_p,kc_b_p);
    SET_RGB(colors[LED_K4][STATE_PRESSED],kd_r_p,kd_g_p,kd_b_p);

    SET_RGB(colors[LED_C1][STATE_PRESSED],ca_r_p,ca_g_p,ca_b_p);
    SET_RGB(colors[LED_C2][STATE_PRESSED],cb_r_p,cb_g_p,cb_b_p);
    SET_RGB(colors[LED_C3][STATE_PRESSED],cc_r_p,cc_g_p,cc_b_p);
    SET_RGB(colors[LED_C4][STATE_PRESSED],cd_r_p,cd_g_p,cd_b_p);
    SET_RGB(colors[LED_C5][STATE_PRESSED],ce_r_p,ce_g_p,ce_b_p);

    SET_RGB(colors[LED_M1][STATE_PRESSED],ma_r_p,ma_g_p,ma_b_p);
    SET_RGB(colors[LED_M2][STATE_PRESSED],mb_r_p,mb_g_p,mb_b_p);
    SET_RGB(colors[LED_M3][STATE_PRESSED],mc_r_p,mc_g_p,mc_b_p);
    SET_RGB(colors[LED_M4][STATE_PRESSED],md_r_p,md_g_p,md_b_p);
    SET_RGB(colors[LED_M5][STATE_PRESSED],me_r_p,me_g_p,me_b_p);

    SET_RGB(colors[LED_ST][STATE_PRESSED],st_r_p,st_g_p,st_b_p);
}


/**
  -----------------------------------------------------
  LED Profile: Reactive
  Color-codes buttons to a reactive profile
  -----------------------------------------------------
*/
void Reactive::run(Kaimana *k, long state) {
    k->setLEDs(k->LED_A1,TO_RGB(colors[k->LED_A1][LED_STATE(state,SB_BTN_AA)]));
    k->setLEDs(k->LED_A2,TO_RGB(colors[k->LED_A2][LED_STATE(state,SB_BTN_AB)]));
    k->setLEDs(k->LED_A3,TO_RGB(colors[k->LED_A3][LED_STATE(state,SB_BTN_AC)]));
    k->setLEDs(k->LED_A4,TO_RGB(colors[k->LED_A4][LED_STATE(state,SB_BTN_AD)]));

    k->setLEDs(k->LED_P1,TO_RGB(colors[k->LED_P1][LED_STATE(state,SB_BTN_PA)]));
    k->setLEDs(k->LED_P2,TO_RGB(colors[k->LED_P2][LED_STATE(state,SB_BTN_PB)]));
    k->setLEDs(k->LED_P3,TO_RGB(colors[k->LED_P3][LED_STATE(state,SB_BTN_PC)]));
    k->setLEDs(k->LED_P4,TO_RGB(colors[k->LED_P4][LED_STATE(state,SB_BTN_PD)]));
    k->setLEDs(k->LED_K1,TO_RGB(colors[k->LED_K1][LED_STATE(state,SB_BTN_KA)]));
    k->setLEDs(k->LED_K2,TO_RGB(colors[k->LED_K2][LED_STATE(state,SB_BTN_KB)]));
    k->setLEDs(k->LED_K3,TO_RGB(colors[k->LED_K3][LED_STATE(state,SB_BTN_KC)]));
    k->setLEDs(k->LED_K4,TO_RGB(colors[k->LED_K4][LED_STATE(state,SB_BTN_KD)]));

    k->setLEDs(k->LED_C1,TO_RGB(colors[k->LED_C1][LED_STATE(state,SB_BTN_CA)]));
    k->setLEDs(k->LED_C2,TO_RGB(colors[k->LED_C2][LED_STATE(state,SB_BTN_CB)]));
    k->setLEDs(k->LED_C3,TO_RGB(colors[k->LED_C3][LED_STATE(state,SB_BTN_CC)]));
    k->setLEDs(k->LED_C4,TO_RGB(colors[k->LED_C4][LED_STATE(state,SB_BTN_CD)]));
    k->setLEDs(k->LED_C5,TO_RGB(colors[k->LED_C5][LED_STATE(state,SB_BTN_CE)]));

    k->setLEDs(k->LED_M1,TO_RGB(colors[k->LED_M1][LED_STATE(state,SB_BTN_MA)]));
    k->setLEDs(k->LED_M2,TO_RGB(colors[k->LED_M2][LED_STATE(state,SB_BTN_MB)]));
    k->setLEDs(k->LED_M3,TO_RGB(colors[k->LED_M3][LED_STATE(state,SB_BTN_MC)]));
    k->setLEDs(k->LED_M4,TO_RGB(colors[k->LED_M4][LED_STATE(state,SB_BTN_MD)]));
    k->setLEDs(k->LED_M5,TO_RGB(colors[k->LED_M5][LED_STATE(state,SB_BTN_ME)]));

    k->setLEDs(k->LED_ST,TO_RGB(colors[k->LED_ST][LED_STATE(state,SB_BTN_ST)]));

    FastLED.delay(10);
}

/**
  -----------------------------------------------------
  Static Lighting configuration
  float breathe_interval: the amount to increase/decrease the brightness
  byte XX_[r,g,b]       : RGB values for the buttons.
                          The buttons correlating to their defined index
  -----------------------------------------------------
*/
Static::Static(
    float breathe_interval,
    byte aa_r, byte aa_g, byte aa_b,
    byte ab_r, byte ab_g, byte ab_b,
    byte ac_r, byte ac_g, byte ac_b,
    byte ad_r, byte ad_g, byte ad_b,

    byte pa_r, byte pa_g, byte pa_b,
    byte pb_r, byte pb_g, byte pb_b,
    byte pc_r, byte pc_g, byte pc_b,
    byte pd_r, byte pd_g, byte pd_b,

    byte ka_r, byte ka_g, byte ka_b,
    byte kb_r, byte kb_g, byte kb_b,
    byte kc_r, byte kc_g, byte kc_b,
    byte kd_r, byte kd_g, byte kd_b,

    byte ca_r, byte ca_g, byte ca_b,
    byte cb_r, byte cb_g, byte cb_b,
    byte cc_r, byte cc_g, byte cc_b,
    byte cd_r, byte cd_g, byte cd_b,
    byte ce_r, byte ce_g, byte ce_b,

    byte ma_r, byte ma_g, byte ma_b,
    byte mb_r, byte mb_g, byte mb_b,
    byte mc_r, byte mc_g, byte mc_b,
    byte md_r, byte md_g, byte md_b,
    byte me_r, byte me_g, byte me_b,

    byte st_r, byte st_g, byte st_b,

    byte mappings[NUM_LEDS/2]
) {
    bInterval=breathe_interval;
    SET_RGB(colors[LED_A1],aa_r,aa_g,aa_b);
    SET_RGB(colors[LED_A2],ab_r,ab_g,ab_b);
    SET_RGB(colors[LED_A3],ac_r,ac_g,ac_b);
    SET_RGB(colors[LED_A4],ad_r,ad_g,ad_b);

    SET_RGB(colors[LED_P1],pa_r,pa_g,pa_b);
    SET_RGB(colors[LED_P2],pb_r,pb_g,pb_b);
    SET_RGB(colors[LED_P3],pc_r,pc_g,pc_b);
    SET_RGB(colors[LED_P4],pd_r,pd_g,pd_b);
    SET_RGB(colors[LED_K1],ka_r,ka_g,ka_b);
    SET_RGB(colors[LED_K2],kb_r,kb_g,kb_b);
    SET_RGB(colors[LED_K3],kc_r,kc_g,kc_b);
    SET_RGB(colors[LED_K4],kd_r,kd_g,kd_b);

    SET_RGB(colors[LED_C1],ca_r,ca_g,ca_b);
    SET_RGB(colors[LED_C2],cb_r,cb_g,cb_b);
    SET_RGB(colors[LED_C3],cc_r,cc_g,cc_b);
    SET_RGB(colors[LED_C4],cd_r,cd_g,cd_b);
    SET_RGB(colors[LED_C5],ce_r,ce_g,ce_b);

    SET_RGB(colors[LED_M1],ma_r,ma_g,ma_b);
    SET_RGB(colors[LED_M2],mb_r,mb_g,mb_b);
    SET_RGB(colors[LED_M3],mc_r,mc_g,mc_b);
    SET_RGB(colors[LED_M4],md_r,md_g,md_b);
    SET_RGB(colors[LED_M5],me_r,me_g,me_b);

    SET_RGB(colors[LED_ST],st_r,st_g,st_b);

}

/**
  -----------------------------------------------------
  LED Profile: Static
  Color-codes buttons to a static profile
  -----------------------------------------------------
*/
void Static::run(Kaimana *k, long state) {
    k->setLEDs(k->LED_A1,TO_RGB(colors[k->LED_A1]));
    k->setLEDs(k->LED_A2,TO_RGB(colors[k->LED_A2]));
    k->setLEDs(k->LED_A3,TO_RGB(colors[k->LED_A3]));
    k->setLEDs(k->LED_A4,TO_RGB(colors[k->LED_A4]));

    k->setLEDs(k->LED_P1,TO_RGB(colors[k->LED_P1]));
    k->setLEDs(k->LED_P2,TO_RGB(colors[k->LED_P2]));
    k->setLEDs(k->LED_P3,TO_RGB(colors[k->LED_P3]));
    k->setLEDs(k->LED_P4,TO_RGB(colors[k->LED_P4]));
    k->setLEDs(k->LED_K1,TO_RGB(colors[k->LED_K1]));
    k->setLEDs(k->LED_K2,TO_RGB(colors[k->LED_K2]));
    k->setLEDs(k->LED_K3,TO_RGB(colors[k->LED_K3]));
    k->setLEDs(k->LED_K4,TO_RGB(colors[k->LED_K4]));

    k->setLEDs(k->LED_C1,TO_RGB(colors[k->LED_C1]));
    k->setLEDs(k->LED_C2,TO_RGB(colors[k->LED_C2]));
    k->setLEDs(k->LED_C3,TO_RGB(colors[k->LED_C3]));
    k->setLEDs(k->LED_C4,TO_RGB(colors[k->LED_C4]));
    k->setLEDs(k->LED_C5,TO_RGB(colors[k->LED_C5]));

    k->setLEDs(k->LED_M1,TO_RGB(colors[k->LED_M1]));
    k->setLEDs(k->LED_M2,TO_RGB(colors[k->LED_M2]));
    k->setLEDs(k->LED_M3,TO_RGB(colors[k->LED_M3]));
    k->setLEDs(k->LED_M4,TO_RGB(colors[k->LED_M4]));
    k->setLEDs(k->LED_M5,TO_RGB(colors[k->LED_M5]));

    k->setLEDs(k->LED_ST,TO_RGB(colors[k->LED_ST]));
}
