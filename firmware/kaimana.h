// vim: set filetype=arduino :
#ifndef kaimana_h
#define kaimana_h

#include "FastLED.h"
#include <stdint.h>
#include "config.h"
#include <ArduinoSTL.h>

#define NUM_LEDS 46
#define DATA_PIN 9

//LED INDEX
#define  LED_A1       mappings[0]
#define  LED_A2       mappings[1]
#define  LED_A3       mappings[2]
#define  LED_A4       mappings[3]

#define  LED_P1       mappings[4]
#define  LED_P2       mappings[5]
#define  LED_P3       mappings[6]
#define  LED_P4       mappings[7]
#define  LED_K4       mappings[8]
#define  LED_K3       mappings[9]
#define  LED_K2       mappings[10]
#define  LED_K1       mappings[11]

#define  LED_C1       mappings[12]
#define  LED_C2       mappings[13]
#define  LED_C3       mappings[14]
#define  LED_C4       mappings[15]
#define  LED_C5       mappings[16]

#define  LED_M1       mappings[17]
#define  LED_M2       mappings[18]
#define  LED_M3       mappings[19]
#define  LED_M4       mappings[20]
#define  LED_M5       mappings[21]

#define  LED_ST       mappings[22]

// This is used to convert a byte[3] containing RGB data to individual arguments
// for the set led function
#define R 0
#define G 1
#define B 2
#define TO_RGB(bytes) bytes[R],bytes[G],bytes[B]

// This is used to inline the logic for button state decoding.
#define STATE_PRESSED 1
#define STATE_RELEASED 0
#define LED_STATE(state,expected) state & expected ? STATE_PRESSED:STATE_RELEASED

// This stores the rgb values in the given color list
#define SET_RGB(color,r, g,b){ \
color[R]=r; \
color[G]=g; \
color[B]=b; \
}


// Define our custom LED colors
#define COLOR_BLACK 0,0,0
#define COLOR_WHITE 255,255,255
#define COLOR_RED 255,0,0
#define COLOR_GREEN 0,255,0
#define COLOR_BLUE 0,0,255
#define COLOR_YELLOW 255,255,0
#define COLOR_ORANGE 255,128,0
#define COLOR_LIME 128,255,0
#define COLOR_TEAL 0,255,255
#define COLOR_PURPLE 128,0,255
#define COLOR_PINK 255,0,255
#define COLOR_HOTPINK 255,0,128
#define COLOR_LIGHTBLUE 204,255,255
#define COLOR_GREY 224,224,224
#define COLOR_FUSCHIA 204,0,204

// Define some default profile helpers so you dont have to manually configure these
// this also maps buttons correctly for Hit Box or Smash Box
#ifdef HITBOX
/**
  -----------------------------------------------------
  Static Hit Box style Layout where the directional buttons
  are a different color than the rest.
  Hit Box Layout Variant
  NOTE: if you wish to use the color macros use `STATIC_HITBOX` instead
  float breathe_interval: interval for breathe effect
  byte r,g,b            : rgb values for the base color
  byte rD,gD,bD         : rgb values for the directional color
  -----------------------------------------------------
*/
#define STATIC_HITBOX_CUSTOM(breathe_interval,r,g,b,rD,gD,bD) \
breathe_interval, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b
/**
  -----------------------------------------------------
  Reactive Hit Box style Layout where the directional buttons
  are a different color than the rest.
  Hit Box Layout Variant
  NOTE: if you wish to use the color macros use `REACTIVE_HITBOX` instead
  float breathe_interval: interval for breathe effect
  byte r,g,b            : rgb values for the unpressed base color
  byte rD,gD,bD         : rgb values for the unpressed directional color
  byte r_p,g_p,b_p      : rgb values for the pressed base color
  byte rD_p,gD_p,bD_p   : rgb values for the pressed directional color
  -----------------------------------------------------
*/
#define REACTIVE_HITBOX_CUSTOM(breathe_interval,r,g,b,rD,gD,bD,r_p,g_p,b_p,rD_p,gD_p,bD_p) \
breathe_interval, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
rD_p,gD_p,bD_p, \
rD_p,gD_p,bD_p, \
rD_p,gD_p,bD_p, \
rD_p,gD_p,bD_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p
#else
/**
  -----------------------------------------------------
  Static Hit Box style Layout where the directional buttons
  are a different color than the rest.
  Hit Box Layout Variant
  NOTE: if you wish to use the color macros use `STATIC_HITBOX` instead
  float breathe_interval: interval for breathe effect
  byte r,g,b            : rgb values for the base color
  byte rD,gD,bD         : rgb values for the directional color
  -----------------------------------------------------
*/
#define STATIC_HITBOX_CUSTOM(breathe_interval,r,g,b,rD,gD,bD) \
breathe_interval, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
r,g,b, \
r,g,b, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
r,g,b
/**
  -----------------------------------------------------
  Reactive Hit Box style Layout where the directional buttons
  are a different color than the rest.
  Smash Box Layout Variant
  NOTE: if you wish to use the color macros use `REACTIVE_HITBOX` instead
  float breathe_interval: interval for breathe effect
  byte r,g,b            : rgb values for the unpressed base color
  byte rD,gD,bD         : rgb values for the unpressed directional color
  byte r_p,g_p,b_p      : rgb values for the pressed base color
  byte rD_p,gD_p,bD_p   : rgb values for the pressed directional color
  -----------------------------------------------------
*/
#define REACTIVE_HITBOX_CUSTOM(breathe_interval,r,g,b,rD,gD,bD,r_p,g_p,b_p,rD_p,gD_p,bD_p) \
breathe_interval, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
r,g,b, \
r,g,b, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
r,g,b, \
rD_p,gD_p,bD_p, \
rD_p,gD_p,bD_p, \
rD_p,gD_p,bD_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
rD_p,gD_p,bD_p, \
rD_p,gD_p,bD_p, \
rD_p,gD_p,bD_p, \
rD_p,gD_p,bD_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
rD_p,gD_p,bD_p, \
rD_p,gD_p,bD_p, \
rD_p,gD_p,bD_p, \
rD_p,gD_p,bD_p, \
r_p,g_p,b_p
/**
  -----------------------------------------------------
  Static Smash Box style Layout where the directional buttons
  and c stick buttonr are a different color than the rest.
  Smash Box Layout Variant
  NOTE: if you wish to use the color macros use `STATIC_SMASHBOX` instead
  float breathe_interval: interval for breathe effect
  byte r,g,b            : rgb values for the base color
  byte rD,gD,bD         : rgb values for the directional color
  byte rC,gC,bC         : rgb values for the c stick color
  -----------------------------------------------------
*/
#define STATIC_SMASHBOX_CUSTOM(breathe_interval,r,g,b,rD,gD,bD,rC,gC,bC) \
breathe_interval, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
rD,gD,bD, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
rC,gC,bC, \
rC,gC,bC, \
rC,gC,bC, \
rC,gC,bC, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b

// Color macro variant of `STATIC_SMASHBOX_CUSTOM`
#define STATIC_SMASHBOX(...) STATIC_SMASHBOX_CUSTOM(__VA_ARGS__)
#endif
/**
  -----------------------------------------------------
  Reactive Solid Layout where the buttons
  are a solid color
  NOTE: if you wish to use the color macros use `REACTIVE_SOLID` instead
  float breathe_interval: interval for breathe effect
  byte r,g,b            : rgb values for the unpressed base color
  byte r_p,g_p,b_p      : rgb values for the pressed directional color
  -----------------------------------------------------
*/
#define REACTIVE_SOLID_CUSTOM(breathe_interval,r,g,b,r_p,g_p,b_p) \
breathe_interval, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p, \
r_p,g_p,b_p
/**
  -----------------------------------------------------
  Static Solid color layout
  NOTE: if you wish to use the color macros use `STATIC_SOLID` instead
  float breathe_interval: interval for breathe effect
  byte r,g,b            : rgb values for the color
  -----------------------------------------------------
*/
#define STATIC_SOLID_CUSTOM(breathe_interval,r,g,b) \
breathe_interval, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b, \
r,g,b

// Color macro variant of `STATIC_HITBOX_CUSTOM`
#define STATIC_HITBOX(...) STATIC_HITBOX_CUSTOM(__VA_ARGS__)
// Color macro variant of `STATIC_SOLID_CUSTOM`
#define STATIC_SOLID(...) STATIC_SOLID_CUSTOM(__VA_ARGS__)
// Color macro variant of `REACTIVE_HITBOX_CUSTOM`
#define REACTIVE_HITBOX(...) REACTIVE_HITBOX_CUSTOM(__VA_ARGS__)
// Color macro variant of `REACTIVE_SOLID_CUSTOM`
#define REACTIVE_SOLID(...) REACTIVE_SOLID_CUSTOM(__VA_ARGS__)

class Kaimana;
/**
  -----------------------------------------------------
  Base class for all profiles
  all members should provide a run function, and set the breathe_interval.
  -----------------------------------------------------
*/
class Profile {
public:
    virtual void run(Kaimana *k,long state) {}
    // run - will execute any led control code using
    // the Kaimana object k and the given state
    virtual float breathe_interval() {
        // breathe_interval - return the breathing interval
        // for the profile
        return bInterval;
    }
protected:
    // breathing interval for the profile
    float bInterval;
};

/**
  -----------------------------------------------------
  Kaimana is the LED control module that interfaces with
  the kaimana J2 modules using FastLED
  -----------------------------------------------------
*/

class Kaimana {
public:
    // Constructors
#ifdef HITBOX
    Kaimana( float brightness,float max_brightness, std::vector<Profile*>profiles,
    byte mp[NUM_LEDS/2]
    );
#else
    Kaimana( float brightness,float max_brightness, std::vector<Profile*>profiles,int8_t sw_profile_1,int8_t sw_profile_2,int8_t sw_profile_3,
    byte mp[NUM_LEDS/2]
    );
#endif

    /****LED control helpers****/
    // this will set the leds at the given index to the given rgb value
    void setLEDs(int btn_index, byte iR, byte iG, byte iB);
    // this will set all leds to the given rgb value
    void setALL(byte iR, byte iG, byte iB);
    /***************************/


    /****Animations****/
    // breath will cause any profile to have a
    // breath animation if the given interval > 0
    void breathe(float interval);
    /******************/

    // configures the leds based on state once.
    // should be run in the main loop
    void run(long state);

#ifndef HITBOX
    //allows for switch based profile configuration
    //returns the profile to use based on state
    //or -2 if the state isnt readable
    inline int8_t profile_id(long state);
#endif
byte mappings[NUM_LEDS/2];

private:
    // list of profiles for the led controller to choose from
    std::vector<Profile*> profiles;

    // default brightness for leds.
    float defaultBrightness;
    // current brightness for leds.
    float bright;
    // max brightness for leds.
    float max_bright;

    // tells the breathe function whether or
    // not its on its way up or down
    bool comeDown;

    // the led objects registered with FastLED
    CRGB leds[NUM_LEDS];

    // current led profile index
    int currentProfileID;
#ifndef HITBOX
    // last known profile switch position
    int8_t lastProfileSwitchState;
    // default profiles for the profile switch
    // position
    int8_t sw_profiles[3];
#endif
};


/**
  -----------------------------------------------------
  Reactive is a version of Profile that allows for reactive lighting modes.
  -----------------------------------------------------
*/
class Reactive: public Profile {
public:
    Reactive(
        float breathe_interval,
        byte aa_r, byte aa_g, byte aa_b,
        byte ab_r, byte ab_g, byte ab_b,
        byte ac_r, byte ac_g, byte ac_b,
        byte ad_r, byte ad_g, byte ad_b,

        byte pa_r, byte pa_g, byte pa_b,
        byte pb_r, byte pb_g, byte pb_b,
        byte pc_r, byte pc_g, byte pc_b,
        byte pd_r, byte pd_g, byte pd_b,

        byte ka_r, byte ka_g, byte ka_b,
        byte kb_r, byte kb_g, byte kb_b,
        byte kc_r, byte kc_g, byte kc_b,
        byte kd_r, byte kd_g, byte kd_b,

        byte ca_r, byte ca_g, byte ca_b,
        byte cb_r, byte cb_g, byte cb_b,
        byte cc_r, byte cc_g, byte cc_b,
        byte cd_r, byte cd_g, byte cd_b,
        byte ce_r, byte ce_g, byte ce_b,

        byte ma_r, byte ma_g, byte ma_b,
        byte mb_r, byte mb_g, byte mb_b,
        byte mc_r, byte mc_g, byte mc_b,
        byte md_r, byte md_g, byte md_b,
        byte me_r, byte me_g, byte me_b,

        byte st_r, byte st_g, byte st_b,

        byte aa_r_p, byte aa_g_p, byte aa_b_p,
        byte ab_r_p, byte ab_g_p, byte ab_b_p,
        byte ac_r_p, byte ac_g_p, byte ac_b_p,
        byte ad_r_p, byte ad_g_p, byte ad_b_p,

        byte pa_r_p, byte pa_g_p, byte pa_b_p,
        byte pb_r_p, byte pb_g_p, byte pb_b_p,
        byte pc_r_p, byte pc_g_p, byte pc_b_p,
        byte pd_r_p, byte pd_g_p, byte pd_b_p,

        byte ka_r_p, byte ka_g_p, byte ka_b_p,
        byte kb_r_p, byte kb_g_p, byte kb_b_p,
        byte kc_r_p, byte kc_g_p, byte kc_b_p,
        byte kd_r_p, byte kd_g_p, byte kd_b_p,

        byte ca_r_p, byte ca_g_p, byte ca_b_p,
        byte cb_r_p, byte cb_g_p, byte cb_b_p,
        byte cc_r_p, byte cc_g_p, byte cc_b_p,
        byte cd_r_p, byte cd_g_p, byte cd_b_p,
        byte ce_r_p, byte ce_g_p, byte ce_b_p,

        byte ma_r_p, byte ma_g_p, byte ma_b_p,
        byte mb_r_p, byte mb_g_p, byte mb_b_p,
        byte mc_r_p, byte mc_g_p, byte mc_b_p,
        byte md_r_p, byte md_g_p, byte md_b_p,
        byte me_r_p, byte me_g_p, byte me_b_p,

        byte st_r_p, byte st_g_p, byte st_b_p,

        byte mappings[NUM_LEDS/2]
    );
    void run(Kaimana *k, long state);
private:
    // list of colors to set each button to based on state colors[led_id][state][r,g,b]
    byte colors[NUM_LEDS/2][2][3];
};

/**
  -----------------------------------------------------
  Reactive is a version of Profile that allows for a static profile
  -----------------------------------------------------
*/
class Static: public Profile {
public:
    Static(
        float breathe_interval,
        byte aa_r, byte aa_g, byte aa_b,
        byte ab_r, byte ab_g, byte ab_b,
        byte ac_r, byte ac_g, byte ac_b,
        byte ad_r, byte ad_g, byte ad_b,

        byte pa_r, byte pa_g, byte pa_b,
        byte pb_r, byte pb_g, byte pb_b,
        byte pc_r, byte pc_g, byte pc_b,
        byte pd_r, byte pd_g, byte pd_b,

        byte ka_r, byte ka_g, byte ka_b,
        byte kb_r, byte kb_g, byte kb_b,
        byte kc_r, byte kc_g, byte kc_b,
        byte kd_r, byte kd_g, byte kd_b,

        byte ca_r, byte ca_g, byte ca_b,
        byte cb_r, byte cb_g, byte cb_b,
        byte cc_r, byte cc_g, byte cc_b,
        byte cd_r, byte cd_g, byte cd_b,
        byte ce_r, byte ce_g, byte ce_b,

        byte ma_r, byte ma_g, byte ma_b,
        byte mb_r, byte mb_g, byte mb_b,
        byte mc_r, byte mc_g, byte mc_b,
        byte md_r, byte md_g, byte md_b,
        byte me_r, byte me_g, byte me_b,

        byte st_r, byte st_g, byte st_b,

        byte mappings[NUM_LEDS/2]
    );
    void run(Kaimana *k, long state);
private:
    // list of colors to set each button to
    byte colors[NUM_LEDS/2][3];
};


#endif
