EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:VCC #PWR0124
U 1 1 5D992820
P 8550 2250
F 0 "#PWR0124" H 8550 2100 50  0001 C CNN
F 1 "VCC" V 8567 2378 50  0000 L CNN
F 2 "" H 8550 2250 50  0001 C CNN
F 3 "" H 8550 2250 50  0001 C CNN
	1    8550 2250
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x05_Male JUSB1
U 1 1 5D995AF1
P 8350 2450
F 0 "JUSB1" H 8458 2831 50  0000 C CNN
F 1 "Brook USB" H 8458 2740 50  0000 C CNN
F 2 "Connector_JST:JST_PH_S5B-PH-K_1x05_P2.00mm_Horizontal" H 8350 2450 50  0001 C CNN
F 3 "~" H 8350 2450 50  0001 C CNN
	1    8350 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 5D999F71
P 8550 2650
F 0 "#PWR0125" H 8550 2400 50  0001 C CNN
F 1 "GND" V 8555 2522 50  0000 R CNN
F 2 "" H 8550 2650 50  0001 C CNN
F 3 "" H 8550 2650 50  0001 C CNN
	1    8550 2650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0126
U 1 1 5D99A40B
P 8550 2550
F 0 "#PWR0126" H 8550 2300 50  0001 C CNN
F 1 "GND" V 8555 2422 50  0000 R CNN
F 2 "" H 8550 2550 50  0001 C CNN
F 3 "" H 8550 2550 50  0001 C CNN
	1    8550 2550
	0    -1   -1   0   
$EndComp
Text GLabel 8550 2350 2    50   Input ~ 0
D-
Text GLabel 8550 2450 2    50   Input ~ 0
D+
NoConn ~ 1000 3800
Wire Wire Line
	6650 1453 6650 1450
Text GLabel 6650 3650 0    50   Input ~ 0
LED_P1
Text GLabel 7100 1650 2    50   Input ~ 0
D+
Text GLabel 7100 1750 2    50   Input ~ 0
D-
Text GLabel 7100 2850 2    50   Input ~ 0
Up
Text GLabel 7100 2750 2    50   Input ~ 0
Down
Text GLabel 7100 2650 2    50   Input ~ 0
Left
Text GLabel 7100 2550 2    50   Input ~ 0
Right
Text GLabel 6650 2550 0    50   Input ~ 0
Ry
Text GLabel 6650 2650 0    50   Input ~ 0
Rx
Text GLabel 6650 2750 0    50   Input ~ 0
Ly
Text GLabel 6650 2850 0    50   Input ~ 0
Lx
$Comp
L power:GND #PWR0136
U 1 1 5EED6314
P 6650 3350
F 0 "#PWR0136" H 6650 3100 50  0001 C CNN
F 1 "GND" V 6655 3222 50  0000 R CNN
F 2 "" H 6650 3350 50  0001 C CNN
F 3 "" H 6650 3350 50  0001 C CNN
	1    6650 3350
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0138
U 1 1 5EED6300
P 7100 1850
F 0 "#PWR0138" H 7100 1700 50  0001 C CNN
F 1 "VCC" V 7118 1977 50  0000 L CNN
F 2 "" H 7100 1850 50  0001 C CNN
F 3 "" H 7100 1850 50  0001 C CNN
	1    7100 1850
	0    1    1    0   
$EndComp
Text GLabel 6650 3250 0    50   Input ~ 0
TPKey
Text GLabel 6650 3850 0    50   Input ~ 0
LED_P3
Text GLabel 6650 3750 0    50   Input ~ 0
LED_P2
Text GLabel 6650 1750 0    50   Input ~ 0
Triangle
Text GLabel 6650 1850 0    50   Input ~ 0
Square
Text GLabel 6650 2050 0    50   Input ~ 0
X
Text GLabel 6650 1950 0    50   Input ~ 0
Circle
Text GLabel 7100 3150 2    50   Input ~ 0
PS
Text GLabel 6650 2150 0    50   Input ~ 0
Share
Text GLabel 7100 2450 2    50   Input ~ 0
Options
Text GLabel 6650 1650 0    50   Input ~ 0
R3
Text GLabel 6650 1550 0    50   Input ~ 0
R1
Text GLabel 7100 2950 2    50   Input ~ 0
L3
Text GLabel 7100 3050 2    50   Input ~ 0
L1
$Comp
L Connector:Conn_01x08_Male X1-B1
U 1 1 5F0B3F53
P 6850 1850
F 0 "X1-B1" H 7100 2250 50  0000 R CNN
F 1 "Conn_01x08_Male" H 6958 2240 50  0001 C CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x08_P2.00mm_Vertical" H 6850 1850 50  0001 C CNN
F 3 "~" H 6850 1850 50  0001 C CNN
	1    6850 1850
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5F0B9AAE
P 6650 1450
F 0 "#PWR0111" H 6650 1200 50  0001 C CNN
F 1 "GND" V 6655 1322 50  0000 R CNN
F 2 "" H 6650 1450 50  0001 C CNN
F 3 "" H 6650 1450 50  0001 C CNN
	1    6650 1450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5F0C425F
P 6650 2350
F 0 "#PWR0112" H 6650 2100 50  0001 C CNN
F 1 "GND" V 6655 2222 50  0000 R CNN
F 2 "" H 6650 2350 50  0001 C CNN
F 3 "" H 6650 2350 50  0001 C CNN
	1    6650 2350
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0137
U 1 1 5EED630A
P 6650 3050
F 0 "#PWR0137" H 6650 2900 50  0001 C CNN
F 1 "+3V3" V 6665 3178 50  0000 L CNN
F 2 "" H 6650 3050 50  0001 C CNN
F 3 "" H 6650 3050 50  0001 C CNN
	1    6650 3050
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x05_Male X1-BUSB1
U 1 1 5F0C8FA2
P 6900 1650
F 0 "X1-BUSB1" H 7100 1950 50  0000 C CNN
F 1 "Brook USB" H 7008 1940 50  0001 C CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x05_P2.00mm_Vertical" H 6900 1650 50  0001 C CNN
F 3 "~" H 6900 1650 50  0001 C CNN
	1    6900 1650
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5F0CF483
P 7100 1550
F 0 "#PWR0113" H 7100 1300 50  0001 C CNN
F 1 "GND" V 7105 1422 50  0000 R CNN
F 2 "" H 7100 1550 50  0001 C CNN
F 3 "" H 7100 1550 50  0001 C CNN
	1    7100 1550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5F0D0243
P 7100 1450
F 0 "#PWR0114" H 7100 1200 50  0001 C CNN
F 1 "GND" V 7105 1322 50  0000 R CNN
F 2 "" H 7100 1450 50  0001 C CNN
F 3 "" H 7100 1450 50  0001 C CNN
	1    7100 1450
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Male X1-B3
U 1 1 5F0E364E
P 6850 3250
F 0 "X1-B3" H 7100 3050 50  0000 R CNN
F 1 "X1-B3" H 6822 3133 50  0001 R CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x02_P2.00mm_Vertical" H 6850 3250 50  0001 C CNN
F 3 "~" H 6850 3250 50  0001 C CNN
	1    6850 3250
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male X1-B4
U 1 1 5F0E8D8A
P 6850 3650
F 0 "X1-B4" H 7100 3350 50  0000 R CNN
F 1 "Conn_01x04_Male" H 6822 3533 50  0001 R CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x04_P2.00mm_Vertical" H 6850 3650 50  0001 C CNN
F 3 "~" H 6850 3650 50  0001 C CNN
	1    6850 3650
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0139
U 1 1 5F0EC65E
P 6650 3550
F 0 "#PWR0139" H 6650 3400 50  0001 C CNN
F 1 "VCC" V 6668 3677 50  0000 L CNN
F 2 "" H 6650 3550 50  0001 C CNN
F 3 "" H 6650 3550 50  0001 C CNN
	1    6650 3550
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x09_Male X1-B5
U 1 1 5F0F1689
P 6900 2750
F 0 "X1-B5" H 7200 3250 50  0000 R CNN
F 1 "Conn_01x09_Male" H 6872 2773 50  0001 R CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x09_P2.00mm_Vertical" H 6900 2750 50  0001 C CNN
F 3 "~" H 6900 2750 50  0001 C CNN
	1    6900 2750
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0140
U 1 1 5F0F9853
P 7100 2350
F 0 "#PWR0140" H 7100 2100 50  0001 C CNN
F 1 "GND" V 7105 2222 50  0000 R CNN
F 2 "" H 7100 2350 50  0001 C CNN
F 3 "" H 7100 2350 50  0001 C CNN
	1    7100 2350
	0    -1   -1   0   
$EndComp
Connection ~ 6650 1450
$Comp
L Connector:Conn_01x08_Male X1-B2
U 1 1 5F0BA120
P 6850 2750
F 0 "X1-B2" H 7100 3100 50  0000 R CNN
F 1 "Conn_01x08_Male" H 6822 2723 50  0001 R CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x08_P2.00mm_Vertical" H 6850 2750 50  0001 C CNN
F 3 "~" H 6850 2750 50  0001 C CNN
	1    6850 2750
	-1   0    0    1   
$EndComp
Wire Wire Line
	9850 1303 9850 1300
Text GLabel 9850 3500 0    50   Input ~ 0
LED_P1
Text GLabel 10300 1500 2    50   Input ~ 0
D+
Text GLabel 10300 1600 2    50   Input ~ 0
D-
Text GLabel 10300 2700 2    50   Input ~ 0
Up
Text GLabel 10300 2600 2    50   Input ~ 0
Down
Text GLabel 10300 2500 2    50   Input ~ 0
Left
Text GLabel 10300 2400 2    50   Input ~ 0
Right
Text GLabel 9850 2400 0    50   Input ~ 0
Ry
Text GLabel 9850 2500 0    50   Input ~ 0
Rx
Text GLabel 9850 2600 0    50   Input ~ 0
Ly
Text GLabel 9850 2700 0    50   Input ~ 0
Lx
$Comp
L power:GND #PWR0116
U 1 1 5F15AD06
P 9850 3200
F 0 "#PWR0116" H 9850 2950 50  0001 C CNN
F 1 "GND" V 9855 3072 50  0000 R CNN
F 2 "" H 9850 3200 50  0001 C CNN
F 3 "" H 9850 3200 50  0001 C CNN
	1    9850 3200
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0117
U 1 1 5F15AD10
P 10300 1700
F 0 "#PWR0117" H 10300 1550 50  0001 C CNN
F 1 "VCC" V 10318 1827 50  0000 L CNN
F 2 "" H 10300 1700 50  0001 C CNN
F 3 "" H 10300 1700 50  0001 C CNN
	1    10300 1700
	0    1    1    0   
$EndComp
Text GLabel 9850 3100 0    50   Input ~ 0
TPKey
Text GLabel 9850 3700 0    50   Input ~ 0
LED_P3
Text GLabel 9850 3600 0    50   Input ~ 0
LED_P2
Text GLabel 9850 1600 0    50   Input ~ 0
Triangle
Text GLabel 9850 1700 0    50   Input ~ 0
Square
Text GLabel 9850 1900 0    50   Input ~ 0
X
Text GLabel 9850 1800 0    50   Input ~ 0
Circle
Text GLabel 10300 3000 2    50   Input ~ 0
PS
Text GLabel 9850 2000 0    50   Input ~ 0
Share
Text GLabel 10300 2300 2    50   Input ~ 0
Options
Text GLabel 9850 1500 0    50   Input ~ 0
R3
Text GLabel 9850 1400 0    50   Input ~ 0
R1
Text GLabel 10300 2800 2    50   Input ~ 0
L3
Text GLabel 10300 2900 2    50   Input ~ 0
L1
$Comp
L Connector:Conn_01x08_Female X2-B1
U 1 1 5F15AD2A
P 10050 1700
F 0 "X2-B1" H 10050 2100 50  0000 R CNN
F 1 "Conn_01x08_Male" H 10158 2090 50  0001 C CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x08_P2.00mm_Vertical" H 10050 1700 50  0001 C CNN
F 3 "~" H 10050 1700 50  0001 C CNN
	1    10050 1700
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0141
U 1 1 5F15AD34
P 9850 1300
F 0 "#PWR0141" H 9850 1050 50  0001 C CNN
F 1 "GND" V 9855 1172 50  0000 R CNN
F 2 "" H 9850 1300 50  0001 C CNN
F 3 "" H 9850 1300 50  0001 C CNN
	1    9850 1300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0142
U 1 1 5F15AD3E
P 9850 2200
F 0 "#PWR0142" H 9850 1950 50  0001 C CNN
F 1 "GND" V 9855 2072 50  0000 R CNN
F 2 "" H 9850 2200 50  0001 C CNN
F 3 "" H 9850 2200 50  0001 C CNN
	1    9850 2200
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0143
U 1 1 5F15AD48
P 9850 2900
F 0 "#PWR0143" H 9850 2750 50  0001 C CNN
F 1 "+3V3" V 9865 3028 50  0000 L CNN
F 2 "" H 9850 2900 50  0001 C CNN
F 3 "" H 9850 2900 50  0001 C CNN
	1    9850 2900
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x05_Female X2-BUSB1
U 1 1 5F15AD52
P 10100 1500
F 0 "X2-BUSB1" H 9900 1800 50  0000 C CNN
F 1 "Brook USB" H 10208 1790 50  0001 C CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x05_P2.00mm_Vertical" H 10100 1500 50  0001 C CNN
F 3 "~" H 10100 1500 50  0001 C CNN
	1    10100 1500
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0144
U 1 1 5F15AD5C
P 10300 1400
F 0 "#PWR0144" H 10300 1150 50  0001 C CNN
F 1 "GND" V 10305 1272 50  0000 R CNN
F 2 "" H 10300 1400 50  0001 C CNN
F 3 "" H 10300 1400 50  0001 C CNN
	1    10300 1400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0145
U 1 1 5F15AD66
P 10300 1300
F 0 "#PWR0145" H 10300 1050 50  0001 C CNN
F 1 "GND" V 10305 1172 50  0000 R CNN
F 2 "" H 10300 1300 50  0001 C CNN
F 3 "" H 10300 1300 50  0001 C CNN
	1    10300 1300
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Female X2-B3
U 1 1 5F15AD70
P 10050 3100
F 0 "X2-B3" H 10050 2900 50  0000 R CNN
F 1 "X1-B3" H 10022 2983 50  0001 R CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x02_P2.00mm_Vertical" H 10050 3100 50  0001 C CNN
F 3 "~" H 10050 3100 50  0001 C CNN
	1    10050 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female X2-B4
U 1 1 5F15AD7A
P 10050 3500
F 0 "X2-B4" H 10050 3200 50  0000 R CNN
F 1 "Conn_01x04_Male" H 10022 3383 50  0001 R CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x04_P2.00mm_Vertical" H 10050 3500 50  0001 C CNN
F 3 "~" H 10050 3500 50  0001 C CNN
	1    10050 3500
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0146
U 1 1 5F15AD84
P 9850 3400
F 0 "#PWR0146" H 9850 3250 50  0001 C CNN
F 1 "VCC" V 9868 3527 50  0000 L CNN
F 2 "" H 9850 3400 50  0001 C CNN
F 3 "" H 9850 3400 50  0001 C CNN
	1    9850 3400
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x09_Female X2-B5
U 1 1 5F15AD8E
P 10100 2600
F 0 "X2-B5" H 10100 3100 50  0000 R CNN
F 1 "Conn_01x09_Male" H 10072 2623 50  0001 R CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x09_P2.00mm_Vertical" H 10100 2600 50  0001 C CNN
F 3 "~" H 10100 2600 50  0001 C CNN
	1    10100 2600
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0147
U 1 1 5F15AD98
P 10300 2200
F 0 "#PWR0147" H 10300 1950 50  0001 C CNN
F 1 "GND" V 10305 2072 50  0000 R CNN
F 2 "" H 10300 2200 50  0001 C CNN
F 3 "" H 10300 2200 50  0001 C CNN
	1    10300 2200
	0    -1   -1   0   
$EndComp
Connection ~ 9850 1300
$Comp
L Connector:Conn_01x08_Female X2-B2
U 1 1 5F15ADA3
P 10050 2600
F 0 "X2-B2" H 10050 3000 50  0000 R CNN
F 1 "Conn_01x08_Male" H 10022 2573 50  0001 R CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x08_P2.00mm_Vertical" H 10050 2600 50  0001 C CNN
F 3 "~" H 10050 2600 50  0001 C CNN
	1    10050 2600
	1    0    0    1   
$EndComp
Text GLabel 6650 2450 0    50   Input ~ 0
R2-T
Text GLabel 6650 2950 0    50   Input ~ 0
L2-T
Text GLabel 9850 2300 0    50   Input ~ 0
R2-T
Text GLabel 9850 2800 0    50   Input ~ 0
L2-T
$EndSCHEMATC
