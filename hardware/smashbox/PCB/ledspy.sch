EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ledspy-rescue:Arduino_Mega2560_Shield-Arduino XAController1
U 1 1 5D58A66E
P 8900 3700
F 0 "XAController1" H 8900 1319 60  0000 C CNN
F 1 "Arduino_Mega2560_Shield" H 8900 1213 60  0000 C CNN
F 2 "Arduino:Arduino_Mega2560_Shield" H 9600 6450 60  0001 C CNN
F 3 "https://store.arduino.cc/arduino-mega-2560-rev3" H 9600 6450 60  0001 C CNN
	1    8900 3700
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0104
U 1 1 5D59F0BB
P 10200 5850
F 0 "#PWR0104" H 10200 5700 50  0001 C CNN
F 1 "VCC" V 10218 5977 50  0000 L CNN
F 2 "" H 10200 5850 50  0001 C CNN
F 3 "" H 10200 5850 50  0001 C CNN
	1    10200 5850
	0    1    -1   0   
$EndComp
NoConn ~ 10200 5750
NoConn ~ 10200 5650
NoConn ~ 10200 5550
NoConn ~ 10200 5450
Wire Wire Line
	10200 4950 10200 5050
Connection ~ 10200 5050
Wire Wire Line
	10200 5050 10200 5150
Connection ~ 10200 5150
Wire Wire Line
	10200 5150 10200 5250
Connection ~ 10200 5250
Wire Wire Line
	10200 5250 10200 5350
$Comp
L power:GND #PWR0106
U 1 1 5D5ACED3
P 10200 5150
F 0 "#PWR0106" H 10200 4900 50  0001 C CNN
F 1 "GND" V 10205 5022 50  0000 R CNN
F 2 "" H 10200 5150 50  0001 C CNN
F 3 "" H 10200 5150 50  0001 C CNN
	1    10200 5150
	0    -1   1    0   
$EndComp
NoConn ~ 10200 4750
NoConn ~ 10200 4650
NoConn ~ 10200 4450
NoConn ~ 10200 4350
NoConn ~ 10200 4250
NoConn ~ 10200 4150
NoConn ~ 10200 4050
NoConn ~ 10200 3950
NoConn ~ 10200 3850
NoConn ~ 10200 3750
NoConn ~ 10200 3650
NoConn ~ 10200 3550
NoConn ~ 10200 3450
NoConn ~ 10200 3350
NoConn ~ 10200 3250
NoConn ~ 10200 3150
NoConn ~ 10200 3050
NoConn ~ 10200 2950
NoConn ~ 10200 2850
NoConn ~ 10200 2550
NoConn ~ 10200 2650
NoConn ~ 10200 2450
NoConn ~ 10200 2350
NoConn ~ 7600 1650
NoConn ~ 7600 1550
NoConn ~ 7600 2350
NoConn ~ 7600 2250
NoConn ~ 7600 2450
NoConn ~ 7600 2650
NoConn ~ 7600 3150
NoConn ~ 7600 3250
NoConn ~ 7600 3750
NoConn ~ 7600 3850
NoConn ~ 7600 5350
NoConn ~ 7600 5750
NoConn ~ 7600 5850
NoConn ~ 10200 2250
NoConn ~ 10200 2150
NoConn ~ 10200 2050
NoConn ~ 10200 1950
NoConn ~ 10200 1850
NoConn ~ 10200 1750
NoConn ~ 10200 1650
NoConn ~ 10200 1550
NoConn ~ 8650 1100
NoConn ~ 8750 1100
NoConn ~ 8850 1100
NoConn ~ 8950 1100
NoConn ~ 9050 1100
NoConn ~ 9150 1100
$Comp
L Connector:Conn_01x04_Male JLED1
U 1 1 5D686D97
P 800 6200
F 0 "JLED1" V 650 6200 50  0000 C CNN
F 1 "Conn_01x04_Male" V 750 6150 39  0000 C CNN
F 2 "Connector_JST:JST_PH_S4B-PH-K_1x04_P2.00mm_Horizontal" H 800 6200 50  0001 C CNN
F 3 "~" H 800 6200 50  0001 C CNN
F 4 "https://lcsc.com/product-detail/Wire-To-Board-Wire-To-Wire-Connector_JST-Sales-America_S4B-PH-K-S-LF-SN_JST-Sales-America-S4B-PH-K-S-LF-SN_C157926.html" V 800 6200 50  0001 C CNN "LCSC"
	1    800  6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 6300 1950 6400
Text Label 1950 6200 2    39   ~ 0
+5V
Text Label 1950 6500 2    39   ~ 0
GND
$Comp
L power:GND #PWR0102
U 1 1 5D59F296
P 1950 6500
F 0 "#PWR0102" H 1950 6250 50  0001 C CNN
F 1 "GND" H 1955 6327 50  0000 C CNN
F 2 "" H 1950 6500 50  0001 C CNN
F 3 "" H 1950 6500 50  0001 C CNN
	1    1950 6500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5D5ACE3E
P 4900 5500
F 0 "R1" V 4900 5500 28  0000 C CNN
F 1 "470" V 4950 5500 28  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4900 5500 50  0001 C CNN
F 3 "~" H 4900 5500 50  0001 C CNN
	1    4900 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 6300 1750 6200
Wire Wire Line
	1750 6200 1950 6200
NoConn ~ 7600 5450
Text GLabel 7600 2050 0    50   Input ~ 0
D7
Text GLabel 7600 2550 0    50   Input ~ 0
D12
Text GLabel 7600 2750 0    50   Input ~ 0
D22
Text GLabel 7600 2850 0    50   Input ~ 0
D23
Text GLabel 7600 2950 0    50   Input ~ 0
D24
Text GLabel 7600 3050 0    50   Input ~ 0
D25
Text GLabel 7600 3350 0    50   Input ~ 0
D28
Text GLabel 7600 3450 0    50   Input ~ 0
D29
Text GLabel 7600 3550 0    50   Input ~ 0
D30
Text GLabel 7600 3650 0    50   Input ~ 0
D31
Text GLabel 7600 3950 0    50   Input ~ 0
D34
Text GLabel 7600 4050 0    50   Input ~ 0
D35
Text GLabel 7600 4150 0    50   Input ~ 0
D36
Text GLabel 7600 4250 0    50   Input ~ 0
D37
Text GLabel 7600 4350 0    50   Input ~ 0
D38
Text GLabel 7600 4450 0    50   Input ~ 0
D39
Text GLabel 7600 4550 0    50   Input ~ 0
D40
Text GLabel 7600 4650 0    50   Input ~ 0
D41
Text GLabel 7600 4750 0    50   Input ~ 0
D42
Text GLabel 7600 4850 0    50   Input ~ 0
D43
Text GLabel 7600 4950 0    50   Input ~ 0
D44
Text GLabel 7600 5050 0    50   Input ~ 0
D45
Text GLabel 7600 5150 0    50   Input ~ 0
D46
Text GLabel 7600 5250 0    50   Input ~ 0
D47
Text GLabel 7600 5550 0    50   Input ~ 0
D50
Text GLabel 7600 5650 0    50   Input ~ 0
D51
Text GLabel 5000 5300 0    50   Input ~ 0
D7
Text GLabel 6600 2800 2    50   Input ~ 0
D12
Text GLabel 6600 1300 2    50   Input ~ 0
D22
Text GLabel 6600 1400 2    50   Input ~ 0
D23
Text GLabel 6600 1500 2    50   Input ~ 0
D24
Text GLabel 6600 1600 2    50   Input ~ 0
D25
Text GLabel 6600 1900 2    50   Input ~ 0
D28
Text GLabel 6600 2000 2    50   Input ~ 0
D29
Text GLabel 6600 3800 2    50   Input ~ 0
D30
Text GLabel 6600 3700 2    50   Input ~ 0
D31
Text GLabel 6600 3400 2    50   Input ~ 0
D34
Text GLabel 6600 3300 2    50   Input ~ 0
D35
Text GLabel 6600 3200 2    50   Input ~ 0
D36
Text GLabel 6600 3100 2    50   Input ~ 0
D37
Text GLabel 6600 4700 2    50   Input ~ 0
D38
Text GLabel 5000 6000 0    50   Input ~ 0
D39
Text GLabel 5000 5900 0    50   Input ~ 0
D40
Text GLabel 5000 5800 0    50   Input ~ 0
D41
Text GLabel 5000 2900 0    50   Input ~ 0
D42
Text GLabel 5000 2800 0    50   Input ~ 0
D43
Text GLabel 5000 2700 0    50   Input ~ 0
D44
Text GLabel 5000 2600 0    50   Input ~ 0
D45
Text GLabel 5000 2500 0    50   Input ~ 0
D46
Text GLabel 5000 2400 0    50   Input ~ 0
D47
Text GLabel 6850 2500 3    50   Input ~ 0
D50
Text GLabel 6850 2400 1    50   Input ~ 0
D51
$Comp
L power:VCC #PWR0101
U 1 1 5E9ED265
P 5800 1000
F 0 "#PWR0101" H 5800 850 50  0001 C CNN
F 1 "VCC" H 5817 1173 50  0000 C CNN
F 2 "" H 5800 1000 50  0001 C CNN
F 3 "" H 5800 1000 50  0001 C CNN
	1    5800 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5E9EDD38
P 5800 6800
F 0 "#PWR0103" H 5800 6550 50  0001 C CNN
F 1 "GND" H 5805 6627 50  0000 C CNN
F 2 "" H 5800 6800 50  0001 C CNN
F 3 "" H 5800 6800 50  0001 C CNN
	1    5800 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1000 5800 1000
$Comp
L Device:C_Small C1
U 1 1 5EA05DD3
P 1300 1550
F 0 "C1" H 1208 1504 50  0000 R CNN
F 1 ".1uF" H 1208 1595 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1300 1550 50  0001 C CNN
F 3 "~" H 1300 1550 50  0001 C CNN
	1    1300 1550
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5EA06CDB
P 1650 1550
F 0 "C2" H 1558 1504 50  0000 R CNN
F 1 ".1uF" H 1558 1595 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1650 1550 50  0001 C CNN
F 3 "~" H 1650 1550 50  0001 C CNN
	1    1650 1550
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5EA0757F
P 2000 1550
F 0 "C3" H 1908 1504 50  0000 R CNN
F 1 ".1uF" H 1908 1595 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2000 1550 50  0001 C CNN
F 3 "~" H 2000 1550 50  0001 C CNN
	1    2000 1550
	-1   0    0    1   
$EndComp
Wire Wire Line
	2000 1450 1650 1450
Connection ~ 1650 1450
Wire Wire Line
	1650 1450 1300 1450
Wire Wire Line
	2000 1650 1650 1650
Connection ~ 1650 1650
Wire Wire Line
	1650 1650 1300 1650
$Comp
L power:VCC #PWR0105
U 1 1 5EA0956B
P 1650 1450
F 0 "#PWR0105" H 1650 1300 50  0001 C CNN
F 1 "VCC" H 1667 1623 50  0000 C CNN
F 2 "" H 1650 1450 50  0001 C CNN
F 3 "" H 1650 1450 50  0001 C CNN
	1    1650 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5EA09C0C
P 1650 1650
F 0 "#PWR0107" H 1650 1400 50  0001 C CNN
F 1 "GND" H 1655 1477 50  0000 C CNN
F 2 "" H 1650 1650 50  0001 C CNN
F 3 "" H 1650 1650 50  0001 C CNN
	1    1650 1650
	1    0    0    -1  
$EndComp
NoConn ~ 5000 3100
NoConn ~ 5000 3200
NoConn ~ 5000 3300
NoConn ~ 5000 3400
NoConn ~ 5000 3500
NoConn ~ 5000 3600
NoConn ~ 5000 3700
NoConn ~ 5000 3800
NoConn ~ 5000 1900
NoConn ~ 5000 2200
NoConn ~ 5000 2300
$Comp
L Connector_Generic:Conn_02x03_Odd_Even ICSP1
U 1 1 5EA17A78
P 1450 2550
F 0 "ICSP1" H 1500 2867 50  0000 C CNN
F 1 "ICSP" H 1500 2776 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 1450 2550 50  0001 C CNN
F 3 "~" H 1450 2550 50  0001 C CNN
	1    1450 2550
	1    0    0    -1  
$EndComp
Text GLabel 1250 2450 0    50   Input ~ 0
MISO
Text GLabel 1250 2550 0    50   Input ~ 0
SCK
Text GLabel 1250 2650 0    50   Input ~ 0
RST
Text GLabel 1750 2550 2    50   Input ~ 0
MOSI
$Comp
L power:GND #PWR0108
U 1 1 5EA1AD63
P 1750 2650
F 0 "#PWR0108" H 1750 2400 50  0001 C CNN
F 1 "GND" V 1755 2522 50  0000 R CNN
F 2 "" H 1750 2650 50  0001 C CNN
F 3 "" H 1750 2650 50  0001 C CNN
	1    1750 2650
	0    -1   -1   0   
$EndComp
$Comp
L power:VCC #PWR0109
U 1 1 5EA1B5AA
P 1750 2450
F 0 "#PWR0109" H 1750 2300 50  0001 C CNN
F 1 "VCC" V 1767 2578 50  0000 L CNN
F 2 "" H 1750 2450 50  0001 C CNN
F 3 "" H 1750 2450 50  0001 C CNN
	1    1750 2450
	0    1    1    0   
$EndComp
Text GLabel 6600 4900 2    50   Input ~ 0
RX0
Connection ~ 5800 1000
$Comp
L MCU_Microchip_ATmega:ATmega2560-16AU U1
U 1 1 5E9B78C7
P 5800 3900
F 0 "U1" H 5950 1100 50  0000 C CNN
F 1 "ATmega2560-16AU" H 6400 1100 50  0000 C CNN
F 2 "Package_QFP:TQFP-100_14x14mm_P0.5mm" H 5800 3900 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-2549-8-bit-AVR-Microcontroller-ATmega640-1280-1281-2560-2561_datasheet.pdf" H 5800 3900 50  0001 C CNN
F 4 "C22460" H 5800 3900 50  0001 C CNN "LCSC"
	1    5800 3900
	1    0    0    -1  
$EndComp
Text GLabel 6600 5000 2    50   Input ~ 0
TX0
NoConn ~ 6600 6500
NoConn ~ 6600 6400
NoConn ~ 6600 6300
NoConn ~ 6600 6200
NoConn ~ 6600 6100
NoConn ~ 6600 6000
NoConn ~ 6600 5900
NoConn ~ 6600 5800
NoConn ~ 6600 5600
NoConn ~ 6600 5500
NoConn ~ 6600 5400
NoConn ~ 6600 5300
NoConn ~ 6600 5100
NoConn ~ 6600 4600
NoConn ~ 6600 4500
NoConn ~ 6600 4400
NoConn ~ 6600 4300
NoConn ~ 6600 4200
NoConn ~ 6600 4100
NoConn ~ 6600 4000
NoConn ~ 5000 5600
NoConn ~ 5000 5100
NoConn ~ 5000 5000
NoConn ~ 5000 4900
NoConn ~ 5000 4700
NoConn ~ 5000 4600
NoConn ~ 5000 4500
NoConn ~ 5000 4400
NoConn ~ 5000 4300
NoConn ~ 5000 4200
NoConn ~ 5000 4100
NoConn ~ 5000 4000
$Comp
L Connector_Generic:Conn_02x03_Odd_Even JEXP1
U 1 1 5EA24EA1
P 1450 3200
F 0 "JEXP1" H 1500 3517 50  0000 C CNN
F 1 "Expansion" H 1500 3426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 1450 3200 50  0001 C CNN
F 3 "~" H 1450 3200 50  0001 C CNN
	1    1450 3200
	1    0    0    -1  
$EndComp
NoConn ~ 7600 2150
NoConn ~ 7600 1950
NoConn ~ 7600 1850
NoConn ~ 7600 1750
Text GLabel 1250 3300 0    50   Input ~ 0
D4
Text GLabel 1250 3200 0    50   Input ~ 0
D5
Text GLabel 1750 3300 2    50   Input ~ 0
D6
Text GLabel 1750 3200 2    50   Input ~ 0
D8
Text GLabel 5000 6300 0    50   Input ~ 0
D4
Text GLabel 6600 5200 2    50   Input ~ 0
D5
Text GLabel 5000 5200 0    50   Input ~ 0
D6
Text GLabel 5000 5400 0    50   Input ~ 0
D8
NoConn ~ 5000 6200
NoConn ~ 5000 6100
NoConn ~ 6600 3600
NoConn ~ 6600 3500
NoConn ~ 6600 2900
NoConn ~ 6600 2200
NoConn ~ 6600 1800
NoConn ~ 6600 1700
$Comp
L Interface_USB:CH340G U2
U 1 1 5EA38D97
P 1850 4650
F 0 "U2" H 2000 4100 50  0000 C CNN
F 1 "CH340G" H 2200 4100 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 1900 4100 50  0001 L CNN
F 3 "http://www.datasheet5.com/pdf-local-2195953" H 1500 5450 50  0001 C CNN
	1    1850 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Male JUSB1
U 1 1 5EA48E80
P 2150 2550
F 0 "JUSB1" H 2258 2931 50  0000 C CNN
F 1 "USB" H 2258 2840 50  0000 C CNN
F 2 "Connector_JST:JST_PH_S5B-PH-K_1x05_P2.00mm_Horizontal" H 2150 2550 50  0001 C CNN
F 3 "~" H 2150 2550 50  0001 C CNN
	1    2150 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 2650 2350 2750
$Comp
L power:GND #PWR0110
U 1 1 5EA4E608
P 2350 2750
F 0 "#PWR0110" H 2350 2500 50  0001 C CNN
F 1 "GND" H 2355 2577 50  0000 C CNN
F 2 "" H 2350 2750 50  0001 C CNN
F 3 "" H 2350 2750 50  0001 C CNN
	1    2350 2750
	1    0    0    -1  
$EndComp
Connection ~ 2350 2750
$Comp
L power:VCC #PWR0111
U 1 1 5EA4ECCC
P 2350 2350
F 0 "#PWR0111" H 2350 2200 50  0001 C CNN
F 1 "VCC" V 2367 2478 50  0000 L CNN
F 2 "" H 2350 2350 50  0001 C CNN
F 3 "" H 2350 2350 50  0001 C CNN
	1    2350 2350
	0    1    1    0   
$EndComp
Text GLabel 2350 2450 2    50   Input ~ 0
D-
Text GLabel 2350 2550 2    50   Input ~ 0
D+
Text GLabel 2700 4950 2    50   Input ~ 0
RST
Text GLabel 4950 1300 1    50   Input ~ 0
RST
Wire Wire Line
	2250 4950 2500 4950
Wire Wire Line
	4800 1100 4800 1000
Connection ~ 4800 1300
Wire Wire Line
	5000 1300 4800 1300
Wire Wire Line
	4800 1300 4750 1300
$Comp
L Device:C_Small C10
U 1 1 5EA10244
P 4800 1200
F 0 "C10" V 4750 1250 50  0000 L CNN
F 1 "22pF" V 4850 1250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4800 1200 50  0001 C CNN
F 3 "~" H 4800 1200 50  0001 C CNN
	1    4800 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5EA0F330
P 4800 1000
F 0 "#PWR0112" H 4800 750 50  0001 C CNN
F 1 "GND" H 4805 827 50  0000 C CNN
F 2 "" H 4800 1000 50  0001 C CNN
F 3 "" H 4800 1000 50  0001 C CNN
	1    4800 1000
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR0113
U 1 1 5EA04959
P 4550 1300
F 0 "#PWR0113" H 4550 1150 50  0001 C CNN
F 1 "VCC" V 4568 1427 50  0000 L CNN
F 2 "" H 4550 1300 50  0001 C CNN
F 3 "" H 4550 1300 50  0001 C CNN
	1    4550 1300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5EA03235
P 4650 1300
F 0 "R2" V 4650 1300 50  0000 C CNN
F 1 "10K" V 4545 1300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4650 1300 50  0001 C CNN
F 3 "~" H 4650 1300 50  0001 C CNN
	1    4650 1300
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C7
U 1 1 5EA52D19
P 2600 4950
F 0 "C7" V 2371 4950 50  0000 C CNN
F 1 ".1uF" V 2462 4950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2600 4950 50  0001 C CNN
F 3 "~" H 2600 4950 50  0001 C CNN
	1    2600 4950
	0    1    1    0   
$EndComp
NoConn ~ 2250 4650
NoConn ~ 2250 4750
NoConn ~ 2250 4850
NoConn ~ 1450 4350
NoConn ~ 2250 4550
NoConn ~ 2250 5050
$Comp
L power:GND #PWR0114
U 1 1 5EA5FE13
P 1850 5250
F 0 "#PWR0114" H 1850 5000 50  0001 C CNN
F 1 "GND" H 1855 5077 50  0000 C CNN
F 2 "" H 1850 5250 50  0001 C CNN
F 3 "" H 1850 5250 50  0001 C CNN
	1    1850 5250
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0115
U 1 1 5EA60A80
P 1850 4050
F 0 "#PWR0115" H 1850 3900 50  0001 C CNN
F 1 "VCC" H 1867 4223 50  0000 C CNN
F 2 "" H 1850 4050 50  0001 C CNN
F 3 "" H 1850 4050 50  0001 C CNN
	1    1850 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5EA6180E
P 1750 3950
F 0 "C4" H 1550 3950 50  0000 L CNN
F 1 "10nF" H 1550 3850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1750 3950 50  0001 C CNN
F 3 "~" H 1750 3950 50  0001 C CNN
	1    1750 3950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5EA627BC
P 1750 3850
F 0 "#PWR0116" H 1750 3600 50  0001 C CNN
F 1 "GND" H 1755 3677 50  0000 C CNN
F 2 "" H 1750 3850 50  0001 C CNN
F 3 "" H 1750 3850 50  0001 C CNN
	1    1750 3850
	-1   0    0    1   
$EndComp
Text GLabel 1450 4550 0    50   Input ~ 0
D+
Text GLabel 1450 4650 0    50   Input ~ 0
D-
Text GLabel 2250 4350 2    50   Input ~ 0
TX0
Text GLabel 2250 4250 2    50   Input ~ 0
RX0
Wire Wire Line
	4600 1700 4700 1700
Wire Wire Line
	4400 1600 4400 1700
Connection ~ 4400 1600
Wire Wire Line
	4400 1500 4400 1600
Connection ~ 4700 1700
Connection ~ 4700 1500
Wire Wire Line
	4700 1500 4600 1500
$Comp
L Device:C_Small C9
U 1 1 5EA79F0C
P 4500 1700
F 0 "C9" V 4600 1550 50  0000 C CNN
F 1 "22pF" V 4600 1700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4500 1700 50  0001 C CNN
F 3 "~" H 4500 1700 50  0001 C CNN
	1    4500 1700
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C8
U 1 1 5EA78DDB
P 4500 1500
F 0 "C8" V 4400 1350 50  0000 C CNN
F 1 "22pF" V 4400 1500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4500 1500 50  0001 C CNN
F 3 "~" H 4500 1500 50  0001 C CNN
	1    4500 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 1500 5000 1500
Wire Wire Line
	5000 1700 4700 1700
$Comp
L power:GND #PWR0117
U 1 1 5EA002AE
P 4400 1600
F 0 "#PWR0117" H 4400 1350 50  0001 C CNN
F 1 "GND" V 4405 1472 50  0000 R CNN
F 2 "" H 4400 1600 50  0001 C CNN
F 3 "" H 4400 1600 50  0001 C CNN
	1    4400 1600
	0    1    1    0   
$EndComp
$Comp
L Device:Crystal_Small Y1
U 1 1 5E9F8B16
P 4700 1600
F 0 "Y1" V 4700 1550 50  0000 L CNN
F 1 "16MHz" V 4700 1650 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_HC49-SD" H 4700 1600 50  0001 C CNN
F 3 "~" H 4700 1600 50  0001 C CNN
F 4 "https://www.digikey.com/product-detail/en/ecs-inc/ECS-160-003-18-5PXEN-TR/XC1696TR-ND/2213798" V 4700 1600 50  0001 C CNN "digikey"
	1    4700 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	1050 5050 1150 5050
Wire Wire Line
	850  4950 850  5050
Connection ~ 850  4950
Wire Wire Line
	850  4850 850  4950
Connection ~ 1150 5050
Connection ~ 1150 4850
Wire Wire Line
	1150 4850 1050 4850
$Comp
L Device:C_Small C6
U 1 1 5EA89257
P 950 5050
F 0 "C6" V 1050 4900 50  0000 C CNN
F 1 "22pF" V 1050 5050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 950 5050 50  0001 C CNN
F 3 "~" H 950 5050 50  0001 C CNN
	1    950  5050
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5EA8925D
P 950 4850
F 0 "C5" V 850 4700 50  0000 C CNN
F 1 "22pF" V 850 4850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 950 4850 50  0001 C CNN
F 3 "~" H 950 4850 50  0001 C CNN
	1    950  4850
	0    1    1    0   
$EndComp
Wire Wire Line
	1150 4850 1450 4850
Wire Wire Line
	1450 5050 1150 5050
$Comp
L power:GND #PWR0118
U 1 1 5EA89265
P 850 4950
F 0 "#PWR0118" H 850 4700 50  0001 C CNN
F 1 "GND" V 855 4822 50  0000 R CNN
F 2 "" H 850 4950 50  0001 C CNN
F 3 "" H 850 4950 50  0001 C CNN
	1    850  4950
	0    1    1    0   
$EndComp
$Comp
L Device:Crystal_Small Y2
U 1 1 5EA8926B
P 1150 4950
F 0 "Y2" V 1150 4900 50  0000 L CNN
F 1 "12MHz" V 1150 5000 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_HC49-SD" H 1150 4950 50  0001 C CNN
F 3 "" H 1150 4950 50  0001 C CNN
F 4 "https://www.digikey.com/product-detail/en/ecs-inc/ECS-120-20-3X-TR/XC1770TR-ND/2676600" V 1150 4950 50  0001 C CNN "Digikey"
	1    1150 4950
	0    1    1    0   
$EndComp
Text GLabel 4800 5500 0    50   Input ~ 0
LED_DATA
$Comp
L power:GND #PWR0119
U 1 1 5EB403EB
P 1750 3100
F 0 "#PWR0119" H 1750 2850 50  0001 C CNN
F 1 "GND" H 1755 2927 50  0000 C CNN
F 2 "" H 1750 3100 50  0001 C CNN
F 3 "" H 1750 3100 50  0001 C CNN
	1    1750 3100
	0    -1   -1   0   
$EndComp
Text GLabel 6950 2500 3    50   Input ~ 0
MISO
Wire Wire Line
	6950 2500 6600 2500
Text GLabel 6600 2300 2    50   Input ~ 0
SCK
Text GLabel 6950 2400 1    50   Input ~ 0
MOSI
Wire Wire Line
	6950 2400 6600 2400
NoConn ~ 6600 2600
NoConn ~ 6600 2700
$Comp
L power:VCC #PWR0120
U 1 1 5EC06129
P 1250 3100
F 0 "#PWR0120" H 1250 2950 50  0001 C CNN
F 1 "VCC" V 1268 3227 50  0000 L CNN
F 2 "" H 1250 3100 50  0001 C CNN
F 3 "" H 1250 3100 50  0001 C CNN
	1    1250 3100
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4007 D1
U 1 1 5EC07368
P 1350 6150
F 0 "D1" V 1304 6229 50  0000 L CNN
F 1 "1N4007" V 1395 6229 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123F" H 1350 5975 50  0001 C CNN
F 3 "" H 1350 6150 50  0001 C CNN
F 4 "https://lcsc.com/product-detail/Diodes-General-Purpose_1N4007_C181127.html" V 1350 6150 50  0001 C CNN "LCSC"
	1    1350 6150
	0    1    1    0   
$EndComp
Connection ~ 1350 6300
Wire Wire Line
	1350 6300 1750 6300
$Comp
L power:VCC #PWR0121
U 1 1 5EC0AFDC
P 1350 6000
F 0 "#PWR0121" H 1350 5850 50  0001 C CNN
F 1 "VCC" H 1367 6173 50  0000 C CNN
F 2 "" H 1350 6000 50  0001 C CNN
F 3 "" H 1350 6000 50  0001 C CNN
	1    1350 6000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male JPWR1
U 1 1 5D68A357
P 2150 6300
F 0 "JPWR1" H 2122 6274 50  0000 R CNN
F 1 "Conn_01x04_Male" H 2122 6183 50  0000 R CNN
F 2 "Connector_JST:JST_PH_S4B-PH-K_1x04_P2.00mm_Horizontal" H 2150 6300 50  0001 C CNN
F 3 "~" H 2150 6300 50  0001 C CNN
F 4 "https://lcsc.com/product-detail/Wire-To-Board-Wire-To-Wire-Connector_JST-Sales-America_S4B-PH-K-S-LF-SN_JST-Sales-America-S4B-PH-K-S-LF-SN_C157926.html" H 2150 6300 50  0001 C CNN "LCSC"
	1    2150 6300
	-1   0    0    -1  
$EndComp
NoConn ~ 1000 6400
Text GLabel 1000 6100 1    39   Input ~ 0
LED_DATA
Wire Wire Line
	1000 6300 1350 6300
$Comp
L power:GND #PWR0122
U 1 1 5EC1F515
P 1000 6200
F 0 "#PWR0122" H 1000 5950 50  0001 C CNN
F 1 "GND" V 1005 6072 50  0000 R CNN
F 2 "" H 1000 6200 50  0001 C CNN
F 3 "" H 1000 6200 50  0001 C CNN
	1    1000 6200
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
